import numpy as np
import nux_utils
import scipy.optimize as opt 

#def find_center(data, x0, radii, phi, span=20, tries=4):
    #"""Find the center of a diffraction pattern
    
    #.. note::
        #Requires the SciPy python package!
    
    #:param data: 2D array containing the diffraction pattern
    #:param x0: array of two elements, indicating the starting guess for the center
    #:param radii: 2-elements array containing the minimum and maximum radius, in pixels, at which the code looks when it searches for center
    #:param phi: 2-elements array containing the minimum and maximum scattering direction at which the code looks when it searches for center
    #:param span: size of the box where the center is searched, centered at coordinates x0
    #:param tries: number of search tries, with random starting coordinates within the search box.
   
    #:returns: 2-elements array containing the center coordinates
    #"""
    #import numpy as np
    #import nux_utils
    #from scipy.optimize import minimize
    
    #radii_vec=np.linspace(radii[0], radii[1], 20)
    #phi_vec=np.linspace(phi[0], phi[1], 50)
    
    #starts=[]
    #for i in range(tries):
        #starts.append([np.random.rand()*span+x0[0]-span/2, np.random.rand()*span+x0[1]-span/2])
    
    
    #def target(X):
        #val = nux_utils.pattern.get_center_value(data, X, radii_vec, phi_vec)
        ##print(X,val)
        #return val
        
    #coords=[]
    #vals=[]
     
    #for start in starts:
        #res=minimize(target,
             #start,
             #method='L-BFGS-B', bounds=[(x0[0]-span/2,x0[0]+span/2),(x0[1]-span/2,x0[1]+span/2)], options={"adaptive":True})
        ##res=minimize(target,
             ##start,
             ##method='Nelder-Mead', options={"adaptive":True})
        #coords.append(res.x)
        #vals.append(target(res.x))
        #print(start, coords[-1], vals[-1])
    
    #minval=np.amin(vals)
    #return coords[vals.index(minval)]

#def find_center(data, x0, radii, phi, span=20):
    #"""Find the center of a diffraction pattern
    
    #.. note::
        #Requires the SciPy python package!
    
    #:param data: 2D array containing the diffraction pattern
    #:param x0: array of two elements, indicating the starting guess for the center
    #:param radii: 2-elements array containing the minimum and maximum radius, in pixels, at which the code looks when it searches for center
    #:param phi: 2-elements array containing the minimum and maximum scattering direction at which the code looks when it searches for center
    #:param span: size of the box, in pixels where the center is searched, centered at coordinates x0
   
    #:returns: 2-elements array containing the center coordinates
    #"""
    #import numpy as np
    #import nux_utils
    #from scipy.optimize import differential_evolution
    
    #radii_vec=np.linspace(radii[0], radii[1], 30)
    #phi_vec=np.linspace(phi[0], phi[1], 50)
    
    #bounds=[(x0[0]-span/2,x0[0]+span/2),(x0[1]-span/2,x0[1]+span/2)]    
    
    #def target(X):
        #val = nux_utils.pattern.get_center_value(data, X, radii_vec, phi_vec)
        ##print(X,val)
        #return val
     
    #result = differential_evolution(target, bounds=bounds, strategy='best1bin', maxiter=1000, popsize=15, mutation=0.5, recombination=0.7, polish=True, updating='immediate', workers=1)
    
    #return result.x

#def find_center_tracking(data, x0, radii, phi, span=20):
    #"""Find the center of a diffraction pattern
    
    #.. note::
        #Requires the SciPy python package!
    
    #:param data: 2D array containing the diffraction pattern
    #:param x0: array of two elements, indicating the starting guess for the center
    #:param radii: 2-elements array containing the minimum and maximum radius, in pixels, at which the code looks when it searches for center
    #:param phi: 2-elements array containing the minimum and maximum scattering direction at which the code looks when it searches for center
    #:param span: size of the box, in pixels where the center is searched, centered at coordinates x0
   
    #:returns: 2-elements array containing the center coordinates
    #"""
    #import numpy as np
    #import nux_utils
    #import nux_utils.pattern
    #from scipy.optimize import differential_evolution
    
    #radii_vec=np.linspace(radii[0], radii[1], 30)
    #phi_vec=np.linspace(phi[0], phi[1], 50)
    
    #bounds=[(x0[0]-span/2,x0[0]+span/2),(x0[1]-span/2,x0[1]+span/2)]    
    
    #def target(X):
        #val = nux_utils.pattern.get_center_value(data, X, radii_vec, phi_vec)
        ##print(X,val)
        #return val
    
    #tracking_x = []
    #tracking_y = []
    #tracking_val = []
    
    #def track(xk, convergence):
        #tracking_x.append(xk[0])
        #tracking_y.append(xk[1])
        #tracking_val.append(target(xk))
     
    #result = differential_evolution(target, bounds=bounds, callback=track, strategy='best1bin', maxiter=1000, popsize=15, mutation=0.5, recombination=0.7, polish=True, updating='immediate', workers=1)
    
    #return result.x, tracking_x, tracking_y, tracking_val





#def find_center_old(data, x0, radii, phi, span=20):
    #"""Find the center of a diffraction pattern
    
    #.. note::
        #Requires the SciPy python package!
    
    #:param data: 2D array containing the diffraction pattern
    #:param x0: array of two elements, indicating the starting guess for the center
    #:param radii: 2-elements array containing the minimum and maximum radius, in pixels, at which the code looks when it searches for center
    #:param phi: 2-elements array containing the minimum and maximum scattering direction at which the code looks when it searches for center
    #:param span: size of the box, in pixels where the center is searched, centered at coordinates x0
   
    #:returns: 2-elements array containing the center coordinates
    #"""
    #import numpy as np
    #import nux_utils
    #from scipy.optimize import differential_evolution, minimize
    
    #radii_vec=np.linspace(radii[0], radii[1], radii[1]-radii[0])
    
    #bounds=[(x0[0]-span/2,x0[0]+span/2),(x0[1]-span/2,x0[1]+span/2)]    
    
    #def target(X):
        
        #profile = nux_utils.pattern.get_radial_profile(data, 1, X, radii_vec, phi)
        #profile_ft = np.abs(np.fft.fft(profile))
        #profile_ft = profile_ft/profile_ft[0]

        #for i in range(1,len(profile_ft)-1):
            #if profile_ft[i]>profile_ft[i-1] and profile_ft[i]>profile_ft[i+1]:
                #return -profile_ft[i]
            ##print(i)
        
        #return 0
    
    #def track(xk, convergence):
        #print(xk, target(xk))
     
    #result = differential_evolution(target, bounds=bounds, callback=track, strategy='best1bin', maxiter=1000, popsize=10, mutation=0.5, recombination=0.7, polish=True, updating='immediate')
    ##result=minimize(target, 
                 ##x0, callback=track,
                 ##method='Nelder-Mead', options={"adaptive":True})
    #return result.x



#def find_center_brute(data, x0, radii, phi, span=20):
    #"""Find the center of a diffraction pattern
    
    #.. note::
        #Requires the SciPy python package!
    
    #:param data: 2D array containing the diffraction pattern
    #:param x0: array of two elements, indicating the starting guess for the center
    #:param radii: 2-elements array containing the minimum and maximum radius, in pixels, at which the code looks when it searches for center
    #:param phi: 2-elements array containing the minimum and maximum scattering direction at which the code looks when it searches for center
    #:param span: size of the box, in pixels where the center is searched, centered at coordinates x0
   
    #:returns: 2-elements array containing the center coordinates
    #"""
    #import numpy as np
    #import nux_utils
    #from scipy.optimize import differential_evolution, minimize
    
    #radii_vec=np.linspace(radii[0], radii[1], radii[1]-radii[0])
    
    #bounds=[(x0[0]-span/2,x0[0]+span/2),(x0[1]-span/2,x0[1]+span/2)]    
    
    
    #centers = []
    #print(bounds[0], bounds[1])
    #for x in np.linspace(bounds[0][0], bounds[0][1], span+1):
        #for y in np.linspace(bounds[1][0], bounds[1][1], span+1):
            #centers.append([x,y])
    
    
    #def target(X):
        
        #profile = nux_utils.pattern.get_radial_profile(data, 1, X, radii_vec, phi)
        #profile_ft = np.abs(np.fft.fft(profile))
        #profile_ft = profile_ft/profile_ft[0]

        #for i in range(1,len(profile_ft)-1):
            #if profile_ft[i]>profile_ft[i-1] and profile_ft[i]>profile_ft[i+1]:
                #return -profile_ft[i]
            ##print(i)
        
        #return 0
    
    #vals=[]
    #for center in centers:
        #vals.append(target(center))
        #print(center, vals[-1])
    
    #return centers[vals.index(min(vals))]



def find_center(data, x0, radii, phi, span=20, tries=20):
    """Find the center of a diffraction pattern
    
    .. note::
        Requires the SciPy python package!
    
    :param data: 2D array containing the diffraction pattern
    :param x0: array of two elements, indicating the starting guess for the center
    :param radii: 2-elements array containing the minimum and maximum radius, in pixels, at which the code looks when it searches for center
    :param phi: 2-elements array containing the minimum and maximum scattering direction at which the code looks when it searches for center
    :param span: size of the box, in pixels where the center is searched, centered at coordinates x0
   
    :returns: 2-elements array containing the center coordinates
    """
    import numpy as np
    import nux_utils
    #import scipy.optimize as opt 
    
    radii_vec=np.linspace(radii[0], radii[1], radii[1]-radii[0])
    
    bounds=[(x0[0]-span/2.,x0[0]+span/2.),(x0[1]-span/2.,x0[1]+span/2.)]    
    
    
    #centers = []
    ##print(bounds[0], bounds[1])
    #for x in np.linspace(bounds[0][0], bounds[0][1], steps):
        #for y in np.linspace(bounds[1][0], bounds[1][1], steps):
            #centers.append([x,y])
    
    
    centers=np.random.rand(tries, 2)
    centers=centers*np.array(span)+np.array([bounds[0][0], bounds[1][0]])
    
    #print(centers)

    profiles = nux_utils.pattern.get_radial_profile_centers(data, 1, centers, radii_vec, phi)
    
    vals=[]
    
    for profile in profiles:
        profile=np.array(profile[1:])-np.array(profile[:-1])
        profile_ft = np.abs(np.fft.fft(profile))
        #profile_ft = profile_ft/profile_ft[0]
        
        val=0
    
        fitspan=2
        #for i in range(fitspan,len(profile_ft)-fitspan):
            #if profile_ft[i]>profile_ft[i-1] and profile_ft[i]>profile_ft[i+1]:
                #x = np.linspace(i-fitspan,i+fitspan, 2*fitspan+1, endpoint=True)
                #y = np.array(profile_ft[i-fitspan:i+fitspan+1])
                #z = np.polyfit(x, y, 2)
                #valtemp = (4. *z[0] *z[2] -z[1]**2)/(4.*z[0])
                #if valtemp>val:
                    #val = valtemp
        for i in range(fitspan,len(profile_ft)-fitspan):
            if profile_ft[i]>profile_ft[i-1] and profile_ft[i]>profile_ft[i+1]:
                tempval=profile_ft[i]+ 0.5*(profile_ft[i-1] +profile_ft[i+1])
                if tempval>val:
                    val = tempval
        #profile_ft=np.sort(profile_ft)            
        vals.append(val)
        #vals.append(np.sum(profile_ft[-fitspan:]))


    best=centers[vals.index(max(vals))]
    print(best, max(vals), span)
    if span<=1:
        return best
    else:
        return find_center(data, best, radii, phi, span=span/2, tries=tries) 


#def find_center_new(data, x0, radii, phi, span=20):
    #"""Find the center of a diffraction pattern
    
    #.. note::
        #Requires the SciPy python package!
    
    #:param data: 2D array containing the diffraction pattern
    #:param x0: array of two elements, indicating the starting guess for the center
    #:param radii: 2-elements array containing the minimum and maximum radius, in pixels, at which the code looks when it searches for center
    #:param phi: 2-elements array containing the minimum and maximum scattering direction at which the code looks when it searches for center
    #:param span: size of the box, in pixels where the center is searched, centered at coordinates x0
   
    #:returns: 2-elements array containing the center coordinates
    #"""
    #import numpy as np
    #import nux_utils
    #from scipy.optimize import differential_evolution, minimize
    
    #radii_vec=np.linspace(radii[0], radii[1], radii[1]-radii[0])
    
    #phi_vec=np.linspace(phi[0], phi[1], 10)
    
    #bounds=[(x0[0]-span/2,x0[0]+span/2),(x0[1]-span/2,x0[1]+span/2)]    
    
    #def target(X):
        
        #profile = []
        
        #for phi_val in phi_vec:
            #profile.append(nux_utils.pattern.get_radial_profile(data, 1, X, radii_vec, phi_val))
        
        #profile_sum=[0]*len(radii_vec)
        #counts=[0]*len(radii_vec)
        
        #for iprof in range(len(profile)):
            #for i in range(len(radii_vec)):
                #if profile[iprof][i]>=0:
                    #profile_sum[i]=profile[iprof][i]
                    #counts[i]+=1
        
        #for i in range(len(profile_sum)):
            #if counts[i]>0:
                #profile_sum[i]=profile_sum[i]/counts[i]


        #profile_ft = np.abs(np.fft.fft(np.log(profile_sum)))
        #profile_ft =profile_ft/profile_ft[0]

        #for i in range(1,len(profile_ft)-1):
            #if profile_ft[i]>=profile_ft[i-1] and profile_ft[i]>=profile_ft[i+1]:
                #return -(profile_ft[i])
            ##print(i)
        
        #return 0
    
    #def track(xk, convergence):
        #print(xk, target(xk))
     
    #result = differential_evolution(target, bounds=bounds, callback=track, strategy='best1bin', maxiter=1000, popsize=10, mutation=0.5, recombination=0.7, polish=True, updating='immediate')
    ##result=minimize(target, 
                 ##x0, callback=track,
                 ##method='Nelder-Mead', options={"adaptive":True})
    #return result.x






#def find_center_corr(data, x0, radii, phi, span=20, tries=20):
    #"""Find the center of a diffraction pattern
    
    #.. note::
        #Requires the SciPy python package!
    
    #:param data: 2D array containing the diffraction pattern
    #:param x0: array of two elements, indicating the starting guess for the center
    #:param radii: 2-elements array containing the minimum and maximum radius, in pixels, at which the code looks when it searches for center
    #:param phi: 2-elements array containing the minimum and maximum scattering direction at which the code looks when it searches for center
    #:param span: size of the box, in pixels where the center is searched, centered at coordinates x0
   
    #:returns: 2-elements array containing the center coordinates
    #"""
    #import numpy as np
    #from nux_utils.pattern import get_polar_image
    #from scipy.optimize import differential_evolution, minimize
        
    #bounds=[(x0[0]-span/2.,x0[0]+span/2.),(x0[1]-span/2.,x0[1]+span/2.)]    
    
    
    #centers=np.random.rand(tries, 2)
    #centers=centers*np.array(span)+np.array([bounds[0][0], bounds[1][0]])
    
    ##print(centers)
    #vals=[]
    #size=200
    
    #for center in centers:
        #print("\t", center)
        #data_polar = get_polar_image(data, 1, center, radii, phi, [size,size])
        #data_polar_ft =  np.abs((np.fft.fft2(data_polar)))
        #data_polar_corr = np.abs(np.fft.fft2(data_polar_ft**2))
        ##data_polar_xcorr = np.sum(data_polar_corr, axis=0)
        ##data_polar_xcorr = data_polar_xcorr/data_polar_xcorr[0]
        ##data_polar_ycorr = np.sum(data_polar_corr, axis=1)
        ##data_polar_ycorr = data_polar_ycorr/data_polar_ycorr[0]
        
        ##vals.append(np.sum(data_polar_ycorr))#+np.sum(data_polar_ycorr))
        #vals.append(np.sum(data_polar_corr)/data_polar_corr[0][0])
    #best=centers[vals.index(min(vals))]
    #print(best, min(vals), span)
    #if span<=1:
        #return best
    #else:
        #return find_center_corr(data, best, radii, phi, span=span/2, tries=tries) 






def get_phase_val(data_polar):
    
    data = np.fft.fft(data_polar, axis=0)

    indexes = [0]*data.shape[1]
    
    maxindex=30
    
    totsum=0
    totcounts=0
    
    sums = [0+0j]*maxindex
    counts = [0]*maxindex
    
    #print(data.shape)
    for iprof in range(data.shape[1]):
        profile_ft=np.abs(data[:30,iprof])
        angle_ft=np.angle(data[:30,iprof])

        val=0
        index=-1
        for iang in range(1,profile_ft.shape[0]-1):
            if profile_ft[iang]>profile_ft[iang-1] and profile_ft[iang]>profile_ft[iang+1]:
                if profile_ft[iang]>val:
                    val=profile_ft[iang]
                    index=iang
                    indexes[iprof]=iang
    
        if index>0:
            #print(angle_ft[index])
            sums[index]+=np.exp(1j*angle_ft[index])
            sums[index-1]+=np.exp(1j*angle_ft[index-1])
            sums[index+1]+=np.exp(1j*angle_ft[index+1])

            counts[index]+=1
            counts[index-1]+=1
            counts[index+1]+=1
       
    counts = np.array(counts)
    sums = np.array(sums)

    inds = counts.argsort()
    
    counts = counts[inds]
    sums = sums[inds]
    
    #print(counts)
    #print(sums)
    
    for i in range(-1, -1-maxindex, -1):
        if counts[i]>3:
            totcounts+=counts[i]
            totsum+=np.abs(sums[i])
            
    

    if totcounts>3:
        return totsum/totcounts
    else:
        return 0


    
def target(X, data, radii, phi, size):
    #print("\t", center)
    data_polar = np.array(nux_utils.pattern.get_polar_image(data, 1, X, radii, phi, size))
    data_polar_diff= data_polar[1:,:]-data_polar[:-1,:]
    return -get_phase_val(data_polar_diff)


def find_center_phase(data, x0, radii, phi, span=20, tries=10, workers=1, size=[256,256]):
    """Find the center of a diffraction pattern. This version is based on "phase locking", and it is slower than the other.
    
    .. note::
        Requires the SciPy python package!
    
    :param data: 2D array containing the diffraction pattern
    :param x0: array of two elements, indicating the starting guess for the center
    :param radii: 2-elements array containing the minimum and maximum radius, in pixels, at which the code looks when it searches for center
    :param phi: 2-elements array containing the minimum and maximum scattering direction at which the code looks when it searches for center
    :param span: size of the box, in pixels where the center is searched, centered at coordinates x0
    :param tries: size of the population for the genetic algorithm (that takes care of the optimization procedure)
    :param workers: nr of workers used for parallelization
    :param size: the size of the polar image. Axis0: radial profile, Axis1: nr of profiles
    
    :returns: 2-elements array containing the center coordinates, and the value of the optimization target
    """
        
        
    #import numpy as np

    
    bounds=[(x0[0]-span/2.,x0[0]+span/2.),(x0[1]-span/2.,x0[1]+span/2.)]    


    #def track(xk, convergence):
        #print(xk, target(xk))
        #print(xk)

    updating_strategy = 'immediate'
    if workers != 1:
         updating_strategy = 'deferred'
    result = opt.differential_evolution(target, args=(data, radii, phi, size), bounds=bounds,  strategy='best1bin', maxiter=10, popsize=tries, mutation=0.5, recombination=0.7, polish=False, updating=updating_strategy, workers=workers)
    
    #result=opt.minimize(target, 
                 #result.x,
                 #method='Nelder-Mead', options={"adaptive":True, "maxiter":10})
    #return result.x
    return result.x, -target(result.x,data, radii, phi, size)
    



