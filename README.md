# NUX utils

This project is a Python module that contains a set of useful functions for data analysis.

For a full documentation, see [here](https://share.phys.ethz.ch/~nux/software/nux_utils/current/docs/)
