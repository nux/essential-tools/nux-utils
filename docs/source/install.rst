Installation
============
For a pre-compiled version of the module, please write me at alcolombo@phys.ethz.ch .

Compiling from source
---------------------

The project repository is hosted on the `ETH Gitlab <https://gitlab.ethz.ch/nux/essential-tools/nux-utils>`_.
The software building system is based on `setuptools <https://setuptools.readthedocs.io/en/latest/>`_. 

Compiling on Linux
~~~~~~~~~~~~~~~~~~
These are the full dependencies:

    :C++ Compiler: Up to now, tested only with GCC 9.3, but it should work with any compiler that supports the C++11 standard
    
No other dependencies are required for the moment. The creation of the python module is made thanks to the `PyBind11 <https://pybind11.readthedocs.io/en/stable/>`_ library. This library is header-only, and it is automatically downloaded at compile time. 

The steps for compiling on linux are as trival as it follows:

.. code-block:: none

    python3 -m pip install git+https://gitlab.ethz.ch/nux/essential-tools/nux-utils.git

This command will automatically download the source code from the gitlab repository, compile and install it in the proper place.
Please, make sure that the python version you use to install :code:`nux_utils` is the same as the one that you use to import it and write your scripts.



Compiling on Windows
~~~~~~~~~~~~~~~~~~~~
.. warning::

    Compile code on Windows is often a pain. Please do it only if you are really motivated and you don't suffer from depression. 

These are the steps that should work (to be read as *worked for me*) for compiling and installing the :code:`nux_utils` python module on windows.


1. Download and install C++ compiler. I used Visual Studio 2019 (but anyone from 2015 should be fine). It can be downloaded for free from `here <https://visualstudio.microsoft.com/downloads/>`_ (Community version).
#. Download and install `Anaconda <https://www.anaconda.com/products/individual>`_, a developement platform for Python, that includes also the Python interpreter. 
#. Download the zip archive of the repository, and extract it
#. Open the Anaconda Prompt program, and navigate to the extracted nux_utils folder.
#. | Within the folder, run the command 
   | :code:`python -m pip install .` 
   | (don't forget the final dot)

.. note::
    
    If you do not have the administration privileges on Windows, you need to add the :code:`--user` option to the install command, that is:
    
    .. code::
        
        python -m pip install . --user
        
