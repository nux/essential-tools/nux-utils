The pattern submodule
======================
        
This submodule provides a set of tools to analyze diffraction patterns. For all functions, at least a 2D numpy array is required as input. All angles has to be interpreted as degrees.

List of functions
------------------

.. automodule:: nux_utils.pattern
    :members:
    :imported-members:
    :exclude-members: get_center_value, get_radial_profile_centers
.. raw:: latex

    \newpage
    
