

    
Quick Start
============
    
    
Quick Start example
--------------------
Here we go through a simple example, which explains how to use the package.
The aim of the following presented code is to load a 2D scattering pattern, make some analysis on it and plot the results.
The diffraction data is loaded from an HDF5 file, via the :code:`h5py` python module: for further instructions visit http://docs.h5py.org/en/stable/ .


Let's start with loading the data from the file :code:`example.h5`, that can be found in the test directory within the repository:

.. code-block:: python

    # import the numpy library, to deal with numerical arrays
    import numpy as np

    # import the h5py module, to deal with .h5 files
    import h5py              


    # set up the file to be read, providing the file name and the access mode ("r" stands for read)
    h5file = h5py.File("example.h5", "r") 

    # read the "pattern" dataset within the "example.h5" file
    data = h5file["pattern"][:]                

The :code:`nux_utils` module will be used to get the radial profiles. In particular, only the required function is imported from the submodule :code:`nux_utils.pattern`.

.. code-block:: python
    
    # import the functions from the nux_utils module
    from nux_utils.pattern import get_radial_profile

    # To use these functions, we first need to define a position for the center of the pattern
    center = [526, 522]

    # Then, we need to define the angle, in degrees, that corresponds to one pixel
    theta_max = 30
    dtheta = (2.*theta_max)/data.shape[0]

    # Now, we need to define an array of scattering angles, which identifies the points where we want to compute the radial profile.
    # Here, we need the radial profile from 10 to 25 degrees scatterin angle, sampled with 100 points.
    angles = np.linspace(10,25, 100)

    # Get the radial profile, in the 90 degrees scattering direction at the given scattering angles
    profile_90 = get_radial_profile(data, dtheta, center, angles, 90)

    # Get another radial profile, in the 220 degrees scattering direction at the given scattering angles
    profile_220 = get_radial_profile(data, dtheta, center, angles, 220)

As last step, let's have a look at the data, by plotting it via the :code:`matplotlib` module.

.. code-block:: python

    # import the pyplot submodule of matplotlib
    import matplotlib.pyplot as plt

    # plot the two profiles
    plt.plot(angles, profile_90, label="profile at 90 degrees")
    plt.plot(angles, profile_220, label="profile at 220 degrees")

    # setup the plot to be fancy
    plt.title("My first radial profile with nux_utils!")
    plt.yscale("log")
    plt.xlabel("Scattering angle (degrees)")
    plt.ylabel("Intensity (a.u.)")
    plt.legend()
    
    plt.show()
    
    
This is what you should expect by running this code:

.. figure:: example.png
   :width: 60 %
..    :alt: map to buried treasure
    
    
.. raw:: latex

    \newpage

    

    
Less quick example
--------------------

The following code performs a more or less full analysis on a diffraction pattern.


.. code-block:: python

    # import the numpy library, to deal with numerical arrays
    import numpy as np

    # import the h5py module, to deal with .h5 files
    import h5py              

    # import the pyplot submodule of matplotlib
    import matplotlib.pyplot as plt

    # import the functions from the nux_utils module
    from nux_utils.pattern import get_radial_profile, find_center, flat_correction

    ####################### Load data

    # set up the file to be read, providing the file name and the access mode ("r" stands for read)
    h5file = h5py.File("example2.h5", "r") 

    # read the pattern dataset within the "example2.h5" file
    data_scratch = h5file["vmi/andor"][6][:]

    # find the center of the diffraction pattern
    center = find_center(data_scratch,  [700,500],  [200,500], [0,360])

    # set the parameters necessary for flat detector corrections
    detector_distance = 67
    pixel_size=0.0756

    # correct data for flat detectors effects
    data, dtheta = flat_correction(data_scratch, center, detector_distance, pixel_size)

    ##################### Start Analysis ############################

    # Now, we need to define an array of scattering angles, which identifies the points where we want to compute the radial profile.
    # Here, we need the radial profile from 5 to 25 degrees scatterin angle, sampled with 100 points.
    angles = np.linspace(5,25, 100)

    phi_1=0
    phi_2=150

    # Get the radial profile, in the phi_1 degrees scattering direction at the given scattering angles
    radial_profile_1 = get_radial_profile(data, dtheta, center, angles, phi_1)

    # Get another radial profile, in the phi_2 degrees scattering direction at the given scattering angles
    radial_profile_2 = get_radial_profile(data,  dtheta, center, angles, phi_2)

    # Get the average radial profile between phi_1 and phi_2 scattering directions
    radial_profile_1_2 = get_radial_profile(data,  dtheta, center, angles, [phi_1, phi_2])

    ##################### Plot Results ######################################

    fig, axs = plt.subplots(1, 3, figsize=(16,5))

    axs[0].set_title("Scratch image")
    axs[0].imshow(data_scratch)
    axs[0].scatter(center[0],center[1], color="red", marker='o')

    axs[1].set_title("Flat-corrected image")
    axs[1].imshow(data)
    axs[1].scatter(center[0],center[1], color="red", marker='o')
    axs[1].plot(np.cos(phi_1/180*np.pi)*angles/dtheta + center[0],np.sin(phi_1/180*np.pi)*angles/dtheta + center[1] , color="green")
    axs[1].plot(np.cos(phi_2/180*np.pi)*angles/dtheta + center[0],np.sin(phi_2/180*np.pi)*angles/dtheta + center[1] , color="orange")

    axs[2].plot(angles, radial_profile_1, label="profile at "+str(phi_1)+" degrees", color="green")
    axs[2].plot(angles, radial_profile_2, label="profile at "+str(phi_2)+" degrees", color="orange")
    axs[2].plot(angles, radial_profile_1_2, label="average profile between "+str(phi_1) + " and " + str(phi_2)+" degrees")
    axs[2].set_title("Radial profiles")
    axs[2].semilogy()
    axs[2].set_xlabel("Scattering angle (degrees)")
    axs[2].set_ylabel("Intensity (a.u.)")
    axs[2].axes.yaxis.set_visible(False)
    axs[2].legend()

    plt.tight_layout()
    plt.show()


.. figure:: example_2.png
    :width: 100 %


    
