####################################################
Welcome to the NUX Utils documentation!
####################################################


Introduction
============

Welcome to the NUX Utils python module documentation! 

The aim of this python module is to provide to the group members an  **easy**, **efficient**, **flexible**, and **reliable** tool to perform data analysis.

    :easy: 
        | It's python! 
        | Just open the Python console, type :code:`import nux_utils`, and you are done.
    :efficient: 
        | It's written in C++! 
        | A compiled language well known to be very efficient and widely used for High Performance Computing software. C++ is not straightforward, but it is hidden under the hood. Unless you decide to put your hands in the module source code, you will never need to deal with C++ code.
    :flexible:
        | It's (still) Python! 
        | Even if the most computationally intensive functions are written in C++, it is still possible to extend the module with pure Python code.
    :reliable:  
        | It's shared! 
        | Sharing the same analysis code among different users enables to find out bugs much faster.


Being a *generic* module, it is divided in submodules. Each submodule contains specific functions that deal with the same topic.

For the moment, these submodules are present:

    :pattern: 
        | provides functions to deal with diffraction patterns, such as computing the radial profile.
    :mie: 
        | provides functions to perform Mie simulations.
    

    
.. warning::

    the module is still in beta version. Use it at your own risk.
    


Please, read the documentation carefully and report any issue to alcolombo@phys.ethz.ch .
Thanks for your help and support!

.. toctree::
   :maxdepth: 4
   
   self 

   install
   quickstart
   pattern
   mie
