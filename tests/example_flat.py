# import the numpy library, to deal with numerical arrays
import numpy as np

# import the h5py module, to deal with .h5 files
import h5py              

# import the pyplot submodule of matplotlib
import matplotlib.pyplot as plt

## set up the file to be read, providing the file name and the access mode ("r" stands for read)
h5file = h5py.File("example2.h5", "r") 

## read the "pattern" dataset within the "example.h5" file
data = np.abs(np.array(h5file["vmi/andor"][6][:]))
#data=data/np.amax(data)
#h5file = h5py.File("example.h5", "r") 

## read the "pattern" dataset within the "example.h5" file
#data = h5file["pattern"][:] 

print(data.shape)

# import the functions from the nux_utils module
#import nux_utils
from nux_utils.pattern import flat_correction

theta_max = 30
dtheta = (2.*theta_max)/np.amax(data.shape)
print(dtheta)
#exit()
# To use these functions, we first need to define a position for the center of the pattern
#center = find_center(data, dtheta, [700,500],  [10,25], [0,180])
center = [718,520]

print(center)

data, dtheta = flat_correction(data,center, 60, 0.08 )

print(dtheta)

plt.imshow(data)
plt.scatter(center[0],center[1], color="red", marker='o')
plt.show()

