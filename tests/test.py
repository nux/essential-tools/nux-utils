
import nux_utils

import numpy as np
import matplotlib.pyplot as plt

import h5py

h5file = h5py.File("example.h5", "r")

data = h5file["pattern"][:]

print(data.shape)
#data = np.ones([1024,1024])

center = [526.5, 526.5]
dtheta = 60./data.shape[0]

print(dtheta)

angles = np.linspace(10,25, 100)
phis = np.linspace(0,180, 100)
profile = nux_utils.pattern.get_radial_profile(data, angles, dtheta, center, 0)
profile2 = nux_utils.pattern.get_radial_profile(data, angles, dtheta, center, 0)

ang_profile= nux_utils.pattern.get_angular_profile(data, angles, dtheta, center, 15.)



mieprof= nux_utils.mie.sphere(10, 0.04,0.01, angles)

print(profile)

fig, ax=plt.subplots(1,4)

ax[0].plot(angles, profile)
ax[1].plot(angles, profile2)
ax[2].plot(angles, ang_profile)
ax[3].plot(angles, mieprof)


#fig, ax=plt.subplots(1,3)

#ax[0].set_yscale("log")
#ax[0].plot(radii, fit_res[4]*profile_sim**fit_res[3])
#ax[0].plot(radii, profile)



#ax[1].plot(profile_sim, profile, 'o')
##ax[0].plot(radii, profile_sim)






#ax[2].plot(center[0], center[1], marker='o', markersize=3, color="red")
#ax[2].imshow(masked_data)


##print()

##plt.plot(center[0], center[1], marker='o', markersize=3, color="red")
plt.show()

