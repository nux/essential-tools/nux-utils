# import the numpy library, to deal with numerical arrays
import numpy as np

# import the h5py module, to deal with .h5 files
import h5py              

# import the pyplot submodule of matplotlib
import matplotlib.pyplot as plt

# import the functions from the nux_utils module
from nux_utils.pattern import get_radial_profile, find_center, flat_correction

####################### Load data

# set up the file to be read, providing the file name and the access mode ("r" stands for read)
h5file = h5py.File("example2.h5", "r") 

# read the pattern dataset within the "example2.h5" file
data_scratch = h5file["vmi/andor"][6][:]

# find the center of the diffraction pattern
center = find_center(data_scratch,  [700,500],  [200,400], [0,360])

# set the parameters necessary for flat detector corrections
detector_distance = 67
pixel_size=0.0756

# correct data for flat detectors effects
data, dtheta = flat_correction(data_scratch, center, detector_distance, pixel_size)

##################### Start Analysis ############################

# Now, we need to define an array of scattering angles, which identifies the points where we want to compute the radial profile.
# Here, we need the radial profile from 5 to 25 degrees scatterin angle, sampled with 100 points.
angles = np.linspace(5,25, 100)

phi_1=0
phi_2=150

# Get the radial profile, in the phi_1 degrees scattering direction at the given scattering angles
radial_profile_1 = get_radial_profile(data, dtheta, center, angles, phi_1)

# Get another radial profile, in the phi_2 degrees scattering direction at the given scattering angles
radial_profile_2 = get_radial_profile(data,  dtheta, center, angles, phi_2)

# Get the average radial profile between phi_1 and phi_2 scattering directions
radial_profile_1_2 = get_radial_profile(data,  dtheta, center, angles, [phi_1, phi_2])

##################### Plot Results ######################################

fig, axs = plt.subplots(1, 3, figsize=(16,5))

axs[0].set_title("Scratch image")
axs[0].imshow(data_scratch)
axs[0].scatter(center[0],center[1], color="red", marker='o')

axs[1].set_title("Flat-corrected image")
axs[1].imshow(data)
axs[1].scatter(center[0],center[1], color="red", marker='o')
axs[1].plot(np.cos(phi_1/180*np.pi)*angles/dtheta + center[0],np.sin(phi_1/180*np.pi)*angles/dtheta + center[1] , color="green")
axs[1].plot(np.cos(phi_2/180*np.pi)*angles/dtheta + center[0],np.sin(phi_2/180*np.pi)*angles/dtheta + center[1] , color="orange")

axs[2].plot(angles, radial_profile_1, label="profile at "+str(phi_1)+" degrees", color="green")
axs[2].plot(angles, radial_profile_2, label="profile at "+str(phi_2)+" degrees", color="orange")
axs[2].plot(angles, radial_profile_1_2, label="average profile between "+str(phi_1) + " and " + str(phi_2)+" degrees")
axs[2].set_title("Radial profiles")
axs[2].semilogy()
axs[2].set_xlabel("Scattering angle (degrees)")
axs[2].set_ylabel("Intensity (a.u.)")
axs[2].axes.yaxis.set_visible(False)
axs[2].legend()

plt.tight_layout()
plt.show()
