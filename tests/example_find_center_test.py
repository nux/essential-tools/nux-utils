# import the numpy library, to deal with numerical arrays
import numpy as np

# import the h5py module, to deal with .h5 files
import h5py              

# import the pyplot submodule of matplotlib
import matplotlib.pyplot as plt

## set up the file to be read, providing the file name and the access mode ("r" stands for read)
h5file = h5py.File("example2.h5", "r") 

## read the "pattern" dataset within the "example.h5" file
data = np.abs(np.array(h5file["vmi/andor"][6][:]))
#data=data/np.amax(data)

#h5file = h5py.File("example.h5", "r") 

## read the "pattern" dataset within the "example.h5" file
#data = h5file["pattern"][:] 

print(data.shape)

# import the functions from the nux_utils module
#import nux_utils
from nux_utils.pattern import get_radial_profile, find_center

theta_max = 30
dtheta = (2.*theta_max)/np.amax(data.shape)
print(dtheta)
#exit()
# To use these functions, we first need to define a position for the center of the pattern
#center = find_center(data,  [520,520],  [200,600], [0,270])

center_wrong = [710,510]
center = find_center(data,  center_wrong,  [100,500], [0,180], span=20)

print(center)

plt.imshow(data)
plt.scatter(center[0],center[1], color="red", marker='o')
plt.scatter(center_wrong[0],center_wrong[1], color="green", marker='x')
plt.show()

#center=[512,512]

# Then, we need to define the angle, in degrees, that corresponds to one pixel

# Now, we need to define an array of scattering angles, which identifies the points where we want to compute the radial profile.
# Here, we need the radial profile from 10 to 25 degrees scatterin angle, sampled with 100 points.
angles = np.linspace(10,25, 100)


phi_1=0
phi_2=360


# Get the radial profile, in the 90 degrees scattering direction at the given scattering angles
#profile = get_radial_profile(data, dtheta, center, angles, [phi_1, phi_2])

#profile_wrong = get_radial_profile(data, dtheta, center_wrong, angles, [phi_1, phi_2])

profile = get_radial_profile(data, dtheta, center, angles, [phi_1, phi_2])

profile_wrong = get_radial_profile(data, dtheta, center_wrong, angles, [phi_1, phi_2])

profile=np.array(profile[1:])-np.array(profile[:-1])

profile_wrong=np.array(profile_wrong[1:])-np.array(profile_wrong[:-1])

profile_ft = np.abs(np.fft.fft(profile))
profile_wrong_ft = np.abs(np.fft.fft(profile_wrong))

# plot the two profiles
plt.plot(profile, label="profile ")
plt.plot(profile_wrong, label="profile wrong")
#plt.yscale("log")
plt.xlabel("Scattering angle (degrees)")
plt.ylabel("Intensity (a.u.)")
plt.legend()

plt.show()


plt.plot( profile_ft, label="profile ")
plt.plot( profile_wrong_ft, label="profile wrong")

# setup the plot to be fancy
#plt.yscale("log")
plt.xlabel("Scattering angle (degrees)")
plt.ylabel("Intensity (a.u.)")
plt.legend()

plt.show()
