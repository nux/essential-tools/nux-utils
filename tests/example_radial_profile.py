# import the numpy library, to deal with numerical arrays
import numpy as np

# import the h5py module, to deal with .h5 files
import h5py              

# import the pyplot submodule of matplotlib
import matplotlib.pyplot as plt

## set up the file to be read, providing the file name and the access mode ("r" stands for read)
h5file = h5py.File("example2.h5", "r") 
data = np.abs(np.array(h5file["vmi/andor"][6][:]))

## read the "pattern" dataset within the "example.h5" file
#data=data/np.amax(data)
#h5file = h5py.File("example.h5", "r") 

## read the "pattern" dataset within the "example.h5" file
#data = h5file["pattern"][:] 

print(data.shape)

# import the functions from the nux_utils module
#import nux_utils
from nux_utils.pattern import get_radial_profile, find_center,get_radial_distribution

theta_max = 30
dtheta = (2.*theta_max)/np.amax(data.shape)
print(dtheta)
#exit()
# To use these functions, we first need to define a position for the center of the pattern
center = find_center(data,  [715,515],  [200,500], [0,360])
#center = [718,520]

print(center)

plt.imshow(data)
plt.scatter(center[0],center[1], color="red", marker='o')
plt.show()

##center=[512,512]

## Then, we need to define the angle, in degrees, that corresponds to one pixel

## Now, we need to define an array of scattering angles, which identifies the points where we want to compute the radial profile.
## Here, we need the radial profile from 10 to 25 degrees scatterin angle, sampled with 100 points.



phi_1=180
phi_2=210

theta_1=8
theta_2=25

for nbins in [20, 50, 100, 200, 1000]:
    angles = np.linspace(theta_1,theta_2, nbins)
    
    profile = get_radial_profile(data,  dtheta, center, angles, [phi_1, phi_2])

    plt.plot(angles, profile, label="nbins = "+str(nbins))



angles, intensities = get_radial_distribution(data,  dtheta, center, [theta_1, theta_2], [phi_1, phi_2])
plt.scatter(angles, intensities, label="raw data", s=0.5)

plt.legend()
#plt.yscale("log")
    
plt.show()



## plot the two profiles
#plt.plot(angles, profile_1, label="profile at "+str(phi_2)+" degrees")

#plt.plot(angles, profile_2, label="average profile between "+str(phi_1) + " and " + str(phi_2)+" degrees")


## setup the plot to be fancy
#plt.title("My first radial profile with nux_utils!")
#plt.xlabel("Scattering angle (degrees)")
#plt.ylabel("Intensity (a.u.)")

#plt.show()
