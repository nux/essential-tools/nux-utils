import nux_utils.mie
import numpy as np
nux_utils.mie.sphere_efficiency
R = 2.e-7
delta = -0.2125
beta = 0.0698
lamb = 5.9074e-8
K=nux_utils.mie.sphere_efficiency(R/lamb, delta, beta)
print("Ksca =", K[1])
print("Csca =", K[1]*np.pi*R**2)
