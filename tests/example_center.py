# import the numpy library, to deal with numerical arrays
import numpy as np

# import the h5py module, to deal with .h5 files
import h5py              

# import the pyplot submodule of matplotlib
import matplotlib.pyplot as plt

# set up the file to be read, providing the file name and the access mode ("r" stands for read)
h5file = h5py.File("example2.h5", "r") 

# read the "pattern" dataset within the "example.h5" file
data = np.array(h5file["vmi/andor"][1][:], dtype=float)

print(data.shape)

plt.imshow(data)
plt.show()

# import the functions from the nux_utils module
#import nux_utils
from nux_utils.pattern import get_radial_profile, find_center_py

theta_max = 30
dtheta = (2.*theta_max)/np.max(data.shape)
# To use these functions, we first need to define a position for the center of the pattern
center = find_center_py(data, dtheta, [400,700], [500,800],  [10,12], [0,360])

print(center)

#center=[512,512]

# Then, we need to define the angle, in degrees, that corresponds to one pixel

# Now, we need to define an array of scattering angles, which identifies the points where we want to compute the radial profile.
# Here, we need the radial profile from 10 to 25 degrees scatterin angle, sampled with 100 points.
angles = np.linspace(10,25, 100)


phi_1=0
phi_2=180


# Get the radial profile, in the 90 degrees scattering direction at the given scattering angles
profile = get_radial_profile(data, dtheta, center, angles, phi_1)

# Get another radial profile, in the 220 degrees scattering direction at the given scattering angles
profile_1 = get_radial_profile(data,  dtheta, center, angles, phi_2)

profile_2 = get_radial_profile(data,  dtheta, center, angles, [phi_1, phi_2])



# plot the two profiles
plt.plot(angles, profile, label="profile at "+str(phi_1)+" degrees")
plt.plot(angles, profile_1, label="profile at "+str(phi_2)+" degrees")

plt.plot(angles, profile_2, label="average profile between "+str(phi_1) + " and " + str(phi_2)+" degrees")


# setup the plot to be fancy
plt.title("My first radial profile with nux_utils!")
plt.yscale("log")
plt.xlabel("Scattering angle (degrees)")
plt.ylabel("Intensity (a.u.)")
plt.legend()

plt.show()
