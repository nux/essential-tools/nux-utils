# import the numpy library, to deal with numerical arrays
import numpy as np

# import the h5py module, to deal with .h5 files
import h5py              

# import the pyplot submodule of matplotlib
import matplotlib.pyplot as plt

## set up the file to be read, providing the file name and the access mode ("r" stands for read)
h5file = h5py.File("example2.h5", "r") 
data = np.abs(np.array(h5file["vmi/andor"][6][:]))
start_center=[710,520]
#h5file = h5py.File("example.h5", "r") 
#data = h5file["pattern"][:] 
#start_center=[520,520]


#def get_peak_index(data):
    
    
    #profile_ft = np.sum(np.abs(data[:50,:]), axis=1)
    
    #index=0
    #tempval=0
    
    #for i in range(3,profile_ft.shape[0]-3):
        #if profile_ft[i]>profile_ft[i-1] and profile_ft[i]>profile_ft[i+1]:
            #if profile_ft[i]>tempval:
                #tempval=profile_ft[i]
                #index=i
        
    #return index


#def get_phases(data):
    #phases=[]
    
    #index=int(get_peak_index(data))
    #print(index)
    #for iprof in range(data.shape[1]):
        #profile_ft=np.abs(data[:50,iprof])
        #profile_phase=np.angle(data[:50,iprof])

        
        #phases.append(profile_phase[index])
    
    #phases=np.array(phases)
    #print(np.abs(np.sum(np.exp(1j*phases))/phases.shape[0]))
    #return phases
        ##if tempval<0
        

def get_maxima(data_polar):
    data = np.fft.fft(data_polar, axis=0)

    indexes = [0]*data.shape[1]
    
    maxindex=30
    
    totsum=0
    totcounts=0
    
    sums = [0+0j]*maxindex
    counts = [0]*maxindex
    
    #print(data.shape)
    for iprof in range(data.shape[1]):
        profile_ft=np.abs(data[:30,iprof])
        angle_ft=np.angle(data[:30,iprof])

        val=0
        index=-1
        for iang in range(1,profile_ft.shape[0]-1):
            if profile_ft[iang]>profile_ft[iang-1] and profile_ft[iang]>profile_ft[iang+1]:
                if profile_ft[iang]>val:
                    val=profile_ft[iang]
                    index=iang
                    indexes[iprof]=iang
    
        if index>0:
            #print(angle_ft[index])
            sums[index]+=np.exp(1j*angle_ft[index])
            counts[index]+=1
            
            
    for i in range(maxindex):
        if counts[i]>2:
            totcounts+=counts[i]
            totsum+=np.abs(sums[i])
    

    #print(totsum/totcounts)
    
    #for phase in phases:
        #print(len(phase))
    return indexes
        #if profile_ft[index]>profile_ft[index-1] and profile_ft[index]>profile_ft[index+1]:
            #phases.append(np.angle(data[index,iprof]))

def get_phases(data):

    profile_ft = np.sum(np.abs(data[:50][:]), axis=1)
    index=0
    tempval=0
    for i in range(3,profile_ft.shape[0]-3):
        if profile_ft[i]>profile_ft[i-1] and profile_ft[i]>profile_ft[i+1]:
            if profile_ft[i]>tempval:
                tempval=profile_ft[i]
                index=i
    
    phases=[]
    #print(index)
    #print(index)
    #print(data.shape)
    for iprof in range(data.shape[1]):
        phases.append(np.angle(data[index,iprof]))
    
    phases=np.array(phases)
    phases=np.unwrap(phases)

    #phases_diff = phases[1:]-phases[:-1]
    #phases_diff[dphases_diff>np.pi]-=2.*np.pi
    #phases_diff[phases_diff<-np.pi]+=2.*np.pi
    return phases

#def get_phases(data):
    
    #profile_ft = np.sum(np.abs(data[:50,:]), axis=1)
    #index=0
    #tempval=0
    #for i in range(3,profile_ft.shape[0]-3):
        #if profile_ft[i]>profile_ft[i-1] and profile_ft[i]>profile_ft[i+1]:
            #if profile_ft[i]>tempval:
                #tempval=profile_ft[i]
                #index=i
    
    #phases=[]
    #print(index)
    #print(data.shape)
    #for iprof in range(data.shape[1]):
        #phases.append(np.angle(data[index,iprof]))
    
    #phases=np.array(phases)
    #return phases

print(data.shape)

# import the functions from the nux_utils module
#import nux_utils
from nux_utils.pattern import get_polar_image, find_center, find_center_phase

theta_max = 30
dtheta = (2.*theta_max)/np.amax(data.shape)
print(dtheta)
#exit()


phi=[0,360]
radius=np.array([100,500])
angles=radius*dtheta

# To use these functions, we first need to define a position for the center of the pattern
#center = find_center_corr(data,  start_center, radius,phi, span=20, tries=50)
#center = find_center(data,  start_center,  radius, phi, span=20, tries=20)
#center_wrong = find_center_phase(data, start_center , radius, phi, span=20)
center = [718,520]
center_wrong=[center[0]-8, center[1]+10]

#center_wrong = [700, 500]
print(center)
print(center_wrong)

data_polar = get_polar_image(data, dtheta, center,angles, phi, [200,200])

data_polar_ft =  np.abs((np.fft.fft2(data_polar)))
data_polar_ft = data_polar_ft/data_polar_ft[0][0]

data_polar_xft =  np.abs((np.fft.fft(data_polar, axis=0)))
data_polar_yft =  np.abs((np.fft.fft(data_polar, axis=1)))

data_polar_xft_arg =  np.angle((np.fft.fft(data_polar, axis=0)))

data_polar_xft_phases = get_phases(np.fft.fft(data_polar, axis=0))

data_polar_corr = np.abs(np.fft.fft2(data_polar_ft**2))
data_polar_xcorr = np.sum(data_polar_corr, axis=0)
data_polar_xcorr = data_polar_xcorr/data_polar_xcorr[0]
data_polar_ycorr = np.sum(data_polar_corr, axis=1)
data_polar_ycorr = data_polar_ycorr/data_polar_ycorr[0]

##########################d

data_polar_wrong = get_polar_image(data, dtheta, center_wrong, angles, phi, [100,200])

data_polar_wrong_ft =  np.abs((np.fft.fft2(data_polar_wrong)))
data_polar_wrong_ft = data_polar_wrong_ft/data_polar_wrong_ft[0][0]

data_polar_wrong_xft =  np.abs((np.fft.fft(data_polar_wrong , axis=0)))
data_polar_wrong_yft =  np.abs((np.fft.fft(data_polar_wrong, axis=1)))
data_polar_wrong_xft_arg =  np.angle((np.fft.fft(data_polar_wrong, axis=0)))

data_polar_wrong_xft_phases = get_phases(np.fft.fft(data_polar_wrong, axis=0))


data_polar_wrong_corr = np.abs(np.fft.fft2(data_polar_wrong_ft**2))
data_polar_wrong_xcorr = np.sum(data_polar_wrong_corr, axis=0)
data_polar_wrong_xcorr = data_polar_wrong_xcorr/data_polar_wrong_xcorr[0]
data_polar_wrong_ycorr = np.sum(data_polar_wrong_corr, axis=1)
data_polar_wrong_ycorr = data_polar_wrong_ycorr/data_polar_wrong_ycorr[0]
#######################d

fig, ax = plt.subplots(2,5, figsize=(18,7))

ax[0][0].set_title(str(int(center[0]))+","+str(int(center[1])))
ax[0][0].imshow(data)
ax[0][0].scatter(center[0],center[1], color="red", marker='o')
ax[0][1].imshow(data_polar)
ax[0][2].imshow(np.log(data_polar_xft))
ax[0][2].plot(get_maxima(data_polar))
ax[0][3].imshow(data_polar_xft_arg)

ax[0][4].plot(data_polar_xft_phases, label="phase at peak")


#ax[0][3].imshow(np.log(data_polar_corr))

#ax[0][4].set_yscale("log")
#ax[0][4].plot(data_polar_xcorr, label="correct x")
#ax[1][4].plot(data_polar_ycorr, label="correct y")

#ax[0][3].plot(data_polar_ft[:,0])
#ax[0][4].plot(data_polar_ft[0,:])


ax[1][0].set_title(str(int(center_wrong[0]))+","+str(int(center_wrong[1])))
ax[1][0].imshow(data)
ax[1][0].scatter(center_wrong[0],center_wrong[1], color="red", marker='o')
ax[1][1].imshow(data_polar_wrong)
ax[1][2].imshow(np.log(data_polar_wrong_xft))
ax[1][2].plot(get_maxima(data_polar_wrong))
ax[1][3].imshow(data_polar_wrong_xft_arg)
ax[1][4].plot(data_polar_wrong_xft_phases, label="phase at peak")





#ax[1][4].plot(np.log(np.sum(data_polar_xft, axis=1)), label="phase at peak")
#ax[1][4].plot(np.log(np.sum(data_polar_xft, axis=1)), label="phase at peak")


#ax[1][4].set_yscale("log")
#ax[0][4].plot(data_polar_wrong_xcorr, label="wrong x")
#ax[1][4].plot(data_polar_wrong_ycorr, label="wrong y")

#ax[0][4].legend()
#ax[1][4].legend()
#ax[1][3].plot(data_polar_wrong_ft[:,0])
#ax[1][4].plot(data_polar_wrong_ft[0,:])

plt.savefig("test.pdf")
plt.show()


