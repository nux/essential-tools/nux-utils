# import the numpy library, to deal with numerical arrays
import numpy as np

# import the h5py module, to deal with .h5 files
import h5py              


# set up the file to be read, providing the file name and the access mode ("r" stands for read)
h5file = h5py.File("example.h5", "r") 

# read the "pattern" dataset within the "example.h5" file
data = h5file["pattern"][:]             


# import the functions from the nux_utils module
from nux_utils.pattern import get_angular_profile

# To use these functions, we first need to define a position for the center of the pattern
center = [526, 522]

# Then, we need to define the angle, in degrees, that corresponds to one pixel
theta_max = 30
dtheta = (2.*theta_max)/data.shape[0]


ang_1=10
ang_2=20

# Now, we need to define an array of scattering angles, which identifies the points where we want to compute the radial profile.
# Here, we need the radial profile from 10 to 25 degrees scatterin angle, sampled with 100 points.
phis = np.linspace(0,360, 100)

# Get the radial profile, in the 90 degrees scattering direction at the given scattering angles
profile = get_angular_profile(data,dtheta, center, phis,  ang_1)

profile_1 = get_angular_profile(data,dtheta, center, phis,  ang_2)

profile_2 = get_angular_profile(data,dtheta, center, phis,  [ang_1,ang_2])


# import the pyplot submodule of matplotlib
import matplotlib.pyplot as plt

# plot the two profiles
plt.plot(phis, profile, label="profile at "+str(ang_1)+" degrees scattering angle")
plt.plot(phis, profile_1, label="profile at "+str(ang_2)+" degrees scattering angle")
plt.plot(phis, profile_2, label="average profile between "+str(ang_1) + " and " + str(ang_2)+" degrees scattering angle")

# setup the plot to be fancy
plt.xlabel("Scattering direction (degrees)")
plt.ylabel("Intensity (a.u.)")
plt.legend()

plt.show()
