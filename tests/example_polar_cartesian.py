# import the numpy library, to deal with numerical arrays
import numpy as np

# import the h5py module, to deal with .h5 files
import h5py              

# import the pyplot submodule of matplotlib
import matplotlib.pyplot as plt

## set up the file to be read, providing the file name and the access mode ("r" stands for read)
h5file = h5py.File("example2.h5", "r") 
data = np.abs(np.array(h5file["vmi/andor"][6][:]))
center=[710,520]
#h5file = h5py.File("example.h5", "r") 
#data = h5file["pattern"][:] 
#center=[520,520]


from nux_utils.pattern import get_polar_image,get_cartesian_image
theta_max = 30
dtheta = (2.*theta_max)/np.amax(data.shape)

phi=[0,360]
radius=np.array([0,500])
angles=radius*dtheta

data_polar = get_polar_image(data, dtheta, center,angles, phi, [2000,2000])


data_cartesian = get_cartesian_image(data_polar, dtheta, center,angles, phi, [data.shape[1], data.shape[0]])


fig, ax = plt.subplots(ncols=3,nrows=1, figsize=(18,7))

ax[0].imshow(data)
ax[1].imshow(data_polar)
ax[2].imshow(data_cartesian)


ax[0].set_title("Original")
ax[1].set_title("Polar")
ax[2].set_title("Cartesian from polar")
plt.savefig("test.pdf")
plt.show()


