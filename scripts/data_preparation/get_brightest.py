import numpy as np
from sfdata import SFDataFiles
import argparse
from PIL import Image
import h5py
import glob
from olaf.parser import Parser
#from spring.utils.setup_data import setup_data
import copy
from nux_utils.pattern import downscale, get_spikes_mask
import os
from tqdm import tqdm
import jungfrau_utils as ju
import matplotlib
matplotlib.use('TkAgg')

#def downscale_pattern(pattern, mask, newdim):
       
def downscale_pattern(data, mask, newsize):
    #newdata2 =   cv2.resize(data, (newsize, newsize), interpolation = cv2.INTER_AREA)    
    #newdata =   skresize(data, (newsize, newsize), anti_aliasing=False, order=0, preserve_range=True)  
    
    tempdata = data+1e3
    tempdata[mask<0.5]=-1
    
    newdata =   downscale(tempdata, [newsize, newsize], interpolation=False)    
    
    newmask = np.ones(newdata.shape)
    newmask[newdata<0]=0
    
    newdata = newdata-1e3
    newdata[newmask==0] = np.nan
    
    
    #print("shape:", data.shape, newdata.shape)
    return newdata, newmask

def remove_spikes(data, mask):
    
    tempdata = np.copy(data)
    tempdata[tempdata<0]=0
    
    tempdata[mask<0.5]=-1
    
    
    
    spikesmask = get_spikes_mask(tempdata, minval=10, upfactor= 5)
    #plt.imshow(spikesmask)
    #plt.show()
    
    outmask = np.copy(mask)
    outmask[spikesmask>0.5]=0
    
    return outmask
    
    
def cut(data, center, size):
    
    newdata = np.copy(data[center[0]-size//2:center[0]+size//2, center[1]-size//2:center[1]+size//2])
    
    return newdata

import numbers

import matplotlib.pyplot as plt
import matplotlib


    
def load_mask(filename):
    

    img = Image.open(filename).convert('L')
    mask=np.array(img, dtype=float)
    mask=mask/np.amax(mask)
    mask[mask<0.5]=0
    mask[mask>=0.5]=1
    
    return mask


gain_file = "/das/work/p19/p19582/retrieve/sf/maloja/data/p19582/raw/JF_pedestals/gainMaps/JF15T08V01.h5"
pedestal_file = "/das/work/p19/p19582/retrieve/sf/maloja/data/p19582/raw/JF_pedestals/20220325_075636.JF15T08V01.res.h5"


def load_pattern_from_sf(filename, pids):
    """
    ....
    """

    with SFDataFiles(filename) as datafile:
        channel_name = "JF15T08V01"
        subset = datafile[[channel_name]]
        channel  = subset[channel_name]
        origpids = list(channel.datasets.pids[:,0])
        
        
        indexes = np.array([origpids.index(i) for i in pids])

        return channel[indexes]

    #with ju.File(filename, gain_file=gain_file, pedestal_file=pedestal_file) as juf:
                
        #origpids = juf["pulse_id"][:]  
        
        ##indexes = np.array([origpids.index(i) for i in pids])

        #indexes = np.array([np.where(origpids==i)[0][0] for i in pids])
        
        

        #return juf[indexes]



parser = argparse.ArgumentParser()

parser.add_argument('runs', help="runnumber", type=int, nargs='+')
parser.add_argument('-f', '--fraction', help='Brightest fracion', type=float)
parser.add_argument('-r','--rescale', dest='rescale', action='store_const',
                        const=True, default=False,
                        help='Rescale data')
#parser.add_argument('acq', help="acqnumber", type=int)

#parser.add_argument('pid', help="pid", type=int)
#parser.add_argument('mask', help="mask")
args = parser.parse_args()

#center = [ 1154, 1104]
center = [ 1156, 1116]

cutsize = 2048
dim=1024
#fraction=0.05
rescale = args.rescale
plot=False

print("Rescale:", rescale)




fraction = args.fraction
if args.fraction is None:
    fraction = 0.05
print("Fraction:", fraction)

runs = args.runs

foldername = "HeteroClusters2022"
foldername+="_B{:d}".format(int(fraction*100))
if rescale==True:
    foldername+="_C{:d}".format(cutsize)
    foldername+="_R{:d}".format(dim)
    
    

outfolder = "/sf/maloja/data/p19582/work/Datasets/"+foldername

if not os.path.exists(outfolder):
   # Create a new directory because it does not exist
   os.makedirs(outfolder)
   #print("The new directory is created!")

print("Output folder:", outfolder)



maskfilename = "/sf/maloja/data/p19582/work/analysis/tools/olaf/configs/conf_files_3.0/masks/mask.tiff"
print("Saving runs", runs)

mask = load_mask(maskfilename)

outmask = []

for run in runs:
    print("Working on Run", run)
    
    analysispath = "/sf/maloja/data/p19582/work/analysis/results/meta/analysis_{:04d}".format(run)+"*_3.0.csv"
    outrun = []
    outacq = []
    outpatterns = []
    outpids = []
    outmask=[]

    outmetadata = []
    
    analysisfiles = glob.glob(analysispath)

    if len(analysisfiles)>0:
        runfolder = os.path.join(outfolder, "Run{:04d}".format(run))
        if not os.path.exists(runfolder):
            os.makedirs(runfolder)
        
        #print(analysisfiles)
        #exit(0)
        for filename in analysisfiles:
            aa = Parser()
            origdata = []
            print("loading", filename)
            origdata += aa.load(filename)

            #print(len(origdata))

            proplist = aa.get_properties(origdata)

            #print(*proplist, sep="\n")



            sorted_data_int=aa.sort(origdata, "intensity")[::-1]
            sorted_data_nlit=aa.sort(origdata, "num_pix_above_noise")[::-1]


            int_thresh = sorted_data_int[int(fraction*len(origdata))]["intensity"]        
            nlit_thresh = sorted_data_nlit[int(fraction*len(origdata))]["num_pix_above_noise"]


            selected_data = []
            
            for dd in origdata:
                if dd["intensity"]>int_thresh or dd["num_pix_above_noise"]>nlit_thresh:
                    selected_data.append(dd)




            outmetadata = selected_data
            outpids = [int(dd["pid"]) for dd in selected_data]
            outrun = [int(dd["run"]) for dd in selected_data]
            outacq = [int(dd["acq"]) for dd in selected_data]
            
            outfilename = os.path.join(runfolder, "Acq{:04d}.h5".format(outacq[0]))
            print("Retrieving Run {:04d} Acq {:04d}".format(run, outacq[0]))
            print("output will be written in:", outfilename)
            
            print("Selected",len(selected_data),"over", len(origdata),"patterns ({:.1f}%)".format(len(selected_data)/len(origdata)*100))
            
            #print(pids)
            
            oldpattname = selected_data[0]["filename"]
            replstr = "/das/work/p19/p19582/retrieve/sf/maloja/data/p19582/raw/"
            oldstr = "/sf/maloja/data/p19582/raw/"
            
            pattname = oldpattname.replace(oldstr, replstr).replace("*", "JF15T08V01")
            
            print("Pattern filename", pattname)
            
            
            locpatterns= load_pattern_from_sf(pattname, np.array(outpids))
            #print("Pattern datatype:", locpatterns.dtype)
            
            outpatterns = []
            

            
            for i in tqdm ( range(locpatterns.shape[0]), desc="Run {:04d} Acq {:04d}".format(run, outacq[0])):
                
                if rescale==True:
                                
                    cutpattern = cut(locpatterns[i], center, cutsize)
                    cutmask = cut(mask, center, cutsize)
                    
                    mask_spikes = remove_spikes(cutpattern, cutmask)
                    outdata, outmask = downscale_pattern(cutpattern, mask_spikes, newsize=dim)
                    
                else:
                    outdata, outmask = locpatterns[i], mask
                
                outpatterns.append(np.copy(outdata))

                
                if plot==True:
                    fig, ax = plt.subplots(2,2, figsize=(16,8))
                    
                    locpattern = np.copy(outdata)
                    locpattern[np.isnan(locpattern)]=0
                    minval = np.amax(locpattern)*10**-3
                    #locpattern[locpattern<=0]=minval
                    plotmask = np.ones(locpattern.shape)
                    plotmask[np.isnan(outdata)]=0
                    ax[0,0].imshow(locpattern, norm=matplotlib.colors.LogNorm(vmin = minval, vmax= np.amax(locpattern)), cmap="inferno")
                    ax[0,1].imshow(locpatterns[i], norm=matplotlib.colors.LogNorm(vmin = minval, vmax= np.amax(locpattern)), cmap="inferno")
                    ax[1,0].imshow(plotmask)
                    ax[1,1].imshow(mask)
                    
                    plt.show()


            outpids = np.array(outpids, dtype=int)
            outrun = np.array(outrun, dtype=int)
            outacq = np.array(outacq, dtype=int)
            outpatterns = np.array(outpatterns, dtype=np.float32)

            print(outpids.shape, outrun.shape,outacq.shape,outpatterns.shape)


            h5file = h5py.File(outfilename, "w")
            h5file.create_dataset("data", data=outpatterns)
            h5file.create_dataset("id", data=outpids)
            h5file.create_dataset("mask", data=outmask.astype(np.float32))
            h5file.create_dataset("run", data=outrun)
            h5file.create_dataset("acq", data=outacq)
            
            for key in outmetadata[0].keys():
                metadata = np.array([dd[key] for dd in outmetadata])
                if isinstance(metadata[0], numbers.Number)==False:
                    metadata = [str(m) for m in metadata]
                    
                #print(key, metadata)
                h5file.create_dataset("online_analysis/"+key, data=metadata)

            h5file.close()

            print("Output written to", outfilename)

    else:
        print("Non analysis file found. Skipping run!")
