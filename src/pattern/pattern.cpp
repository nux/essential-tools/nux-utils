#include <vector>

#define _USE_MATH_DEFINES
#include <cmath>
#include <algorithm>
#include <stdio.h>
#include "pattern.h"
#include <random>

#define INNER_STEPS 10

#define WFRAC  0.6

void get_stddev(std::vector<double> data, double& average, double& stddev){
    
    double sum=0;
    double ssum=0;
    double count=0;
    
    for(auto val: data) if(val>=0){
        sum+=val;
        count+=1.;
    }
    
    if(count>2){
        average=sum/count;

        for(auto val: data) if(val>=0){
            ssum+=(val-average)*(val-average);
        
        }
        stddev=std::sqrt(ssum/(count-1));
    
    }
    else{
        average=-1;
        stddev=-1;
    } 
    
    
}




void get_absdev(std::vector<double> data, double& average, double& absdev){
    
    double sum=0;
    double ssum=0;
    double count=0;
    
    for(auto val: data) if(val>=0){
        sum+=val;
        count+=1.;
    }
    
    if(count>2){
        average=sum/count;

        for(auto val: data) if(val>=0){
            ssum+=std::abs(val-average);
        
        }
        absdev=ssum/(count-1);
    
    }
    else{
        average=-1;
        absdev=-1;
    } 
    
    
}


// // // // // // // // // // // // // // // // // // // // // // // // // 

double Pattern::get_data(std::array<std::size_t,2> coords){
    std::size_t index = coords[0] + coords[1] * this->dims[0];
    
//     printf("(%d,%d-->%d)\n", coords[0], coords[1], index);

    if(index>=this->dims[1] * this->dims[0]) return -1;
    else return this->data[index];        
    
}



double Pattern::get_value(std::array<double,2> coords){
    double outval=-1;

    if(coords[0]>=0 && coords[0]<double(this->dims[0]-1) && coords[1]>=0 && coords[1]<double(this->dims[1]-1) ){

        std::array<double,2> coords_rel = {coords[0]-std::floor(coords[0]), coords[1]-std::floor(coords[1])};
        std::array<std::size_t,2> index = {(std::size_t) std::floor(coords[0]), (std::size_t) std::floor(coords[1])};
//         printf("%f,%f   %d,%d\n",coords_rel[0], coords_rel[1], index[0], index[1]);
        double norm=0;
        double val=0;
        if(get_data({index[0], index[1]})>=0){
            double factor = (1.-coords_rel[0])*(1.-coords_rel[1]);
            val  +=  factor * get_data({index[0], index[1]});
            norm +=  factor;
        }
        
        if(get_data({index[0]+1, index[1]})>=0){
            double factor = (coords_rel[0])*(1.-coords_rel[1]);
            val  +=  factor * get_data({index[0]+1, index[1]});
            norm +=  factor;
        }
        
        if(get_data({index[0], index[1]+1})>=0){
            double factor = (1.-coords_rel[0])*(coords_rel[1]);
            val  +=  factor * get_data({index[0], index[1]+1});
            norm +=  factor;
        }
        
        if(get_data({index[0]+1, index[1]+1})>=0){
            double factor = (coords_rel[0])*(coords_rel[1]);
            val  +=  factor * get_data({index[0]+1, index[1]+1});
            norm +=  factor;
        }
            
        if(norm>0)  outval= val/norm;
        else outval=-1;
    }
    
//     printf("(%d,%d) %f,%f-->%f\n", this->dims[0], this->dims[1], coords[0],coords[1], outval);
    return outval;
    
}



double Pattern::get_value_polar(std::array<double,2> coords_polar){
    std::array<double,2>  coords= {coords_polar[0] * std::cos(coords_polar[1]/180.*M_PI)/this->dtheta + this->center[0],
                        coords_polar[0] * std::sin(coords_polar[1]/180.*M_PI)/this->dtheta + this->center[1]};

//     printf("(%d,%d),(%f,%f)  %f,%f-->%f,%f\n", this->dims[0], this->dims[1], this->center[0], this->center[1], coords_polar[0],coords_polar[1], coords[0], coords[1]);

    return get_value(coords);
    }


    

    
std::vector<double> Pattern::get_radial_profile(std::vector<double> angles, double phi){
    
    std::vector<double> profile(angles.size(), 0);
    std::vector<double> norm(angles.size(), 0);
    
    #pragma omp parallel for schedule(dynamic)
    for( std::size_t iang=0; iang<angles.size()-1; iang++){
            double dang=(angles[iang+1]-angles[iang])/double(INNER_STEPS);
//             exit(-1);
            std::size_t limit = INNER_STEPS;
            if(iang==angles.size()-1) limit++;
            
            for(std::size_t iin=0; iin<limit; iin++){

                double val=this->get_value_polar({angles[iang]+dang*double(iin), phi});

                if(val>0){
                    double factor=1.-dang*double(iin)/(angles[iang+1]-angles[iang]);
                
                    #pragma omp atomic
                    profile[iang]+=factor*val;
                    
                    #pragma omp atomic
                    norm[iang]+=factor;
                    
                    #pragma omp atomic
                    profile[iang+1]+=(1.-factor)*val;
                    
                    #pragma omp atomic
                    norm[iang+1]+=(1.-factor);
                    
                }
                
            }
    }
    
    
    for( std::size_t iang=0; iang<angles.size(); iang++){
            if(norm[iang]>0) profile[iang]/=norm[iang];
            else profile[iang]=-1;
        }
    
    return profile;  
    
}


std::vector<double> Pattern::get_radial_profile(std::vector<double> angles, std::array<double,2> phi_limits){
    
    std::vector<double> profile(angles.size(), 0);
    std::vector<double> norm(angles.size(), 0);
    
    
    #pragma omp parallel for schedule(dynamic) collapse(2)
    for(std::size_t y = 0; y<this->dims[1]; y++)
        for(std::size_t x = 0; x<this->dims[0]; x++){
            double xrel=(x-this->center[0]);
            double yrel=(y-this->center[1]);
            
            
            double angle=std::atan2(yrel, xrel)/M_PI*180.;
            while(angle+360. <= phi_limits[1]){ angle+=360.;}
            
            if(angle<=phi_limits[1] && angle>=phi_limits[0]){
                double radius = std::sqrt(xrel*xrel + yrel*yrel)*this->dtheta;
                int rad_index=-1;
//                 int ok=-1;
                
                for(int i=0; i<angles.size()-1; i++){
                    if(angles[i]<=radius && angles[i+1]>radius){
                        rad_index=i;
//                         printf("%d \n", rad_index);
                        break;                    
                    }
                }    
                
                
                if(rad_index>=0){
                    double val=this->get_data({x,y});
                    if(val>=0){
                        double factor=(angles[rad_index+1]-radius)/(angles[rad_index+1]-angles[rad_index]);
        
                        #pragma omp atomic
                        profile[rad_index] += val*factor;
                        
                        #pragma omp atomic
                        norm[rad_index] +=factor;
                        
                        #pragma omp atomic
                        profile[rad_index+1] += val*(1.-factor);
                        
                        #pragma omp atomic
                        norm[rad_index+1] +=(1.-factor);                    
                    }
                    
                    
                }

            }
            
            
        }
        
        
    
    for( std::size_t iang=0; iang<angles.size(); iang++){
            if(norm[iang]>0) profile[iang]/=norm[iang];
            else profile[iang]=-1;
        }
    
    return profile;  
    
}




std::vector<double> Pattern::get_radial_maxima(std::vector<double> angles, std::array<double,2> phi_limits){
    
    std::vector<double> profile(angles.size(), -1);
    std::vector<int> saturated(angles.size(), 0);
    
    
    #pragma omp parallel for schedule(dynamic) collapse(2)
    for(std::size_t y = 0; y<this->dims[1]; y++)
        for(std::size_t x = 0; x<this->dims[0]; x++){
            double xrel=(x-this->center[0]);
            double yrel=(y-this->center[1]);
            
            
            double angle=std::atan2(yrel, xrel)/M_PI*180.;
            while(angle+360. <= phi_limits[1]){ angle+=360.;}
            
            if(angle<=phi_limits[1] && angle>=phi_limits[0]){
                double radius = std::sqrt(xrel*xrel + yrel*yrel)*this->dtheta;
                int rad_index=-1;
//                 int ok=-1;
                
                for(int i=0; i<angles.size()-1; i++){
                    if(angles[i]<=radius && angles[i+1]>radius){
                        rad_index=i;
//                         printf("%d \n", rad_index);
                        break;                    
                    }
                }    
                
                
                if(rad_index>=0){
                    double val=this->get_data({x,y});
                    if(val>profile[rad_index]) profile[rad_index] = val;
                    if(val<-1.1) saturated[rad_index] = 1;
                                
                    
                    
                    
                }

            }
            
            
        }
        
   for( std::size_t iang=0; iang<angles.size(); iang++) if(saturated[iang]>0) profile[iang]=-1;
    return profile;  
    
}



std::vector<std::vector<double>> Pattern::get_polar_image(std::array<double,2> angle_limits, std::array<double,2> phi_limits,  int nangles, int nphi){
    
    std::vector<std::vector<double>> image(nangles, std::vector<double>(nphi,0));
    
    double dangle=(angle_limits[1]-angle_limits[0])/double(nangles);
    double dphi=(phi_limits[1]-phi_limits[0])/double(nphi);
    
    int angle_sampling_steps=int(std::ceil(dangle/this->dtheta));
    // int angle_sampling_steps=1;
    double dangle_sampling=dangle/double(angle_sampling_steps);
    
    #pragma omp parallel for schedule(dynamic)
    for(std::size_t iangle = 0; iangle<nangles; iangle++){
        double minangle = dangle*double(iangle)+dangle_sampling/2.+angle_limits[0];
        
        for(std::size_t iphi = 0; iphi<nphi; iphi++){
//             double dphi_sampling=dangle_sampling/this->dtheta;
            int phi_sampling_steps= int(std::ceil((minangle*dphi/180.*M_PI)/dangle_sampling));
//             int phi_sampling_steps= int(std::ceil(dphi/dphi_sampling));
            double dphi_sampling=dphi/double(phi_sampling_steps);
//             double maxangle = dangle*double(iangle+1)+dangle/2.;
            double minphi = dphi*double(iphi)+dphi_sampling/2.+phi_limits[0];
//             double maxphi = dphi*double(iphi+1)+dphi/2.; 
            
            
//             printf("angle=%f,dangle=%f, dangle_sampling=%f, dangle_nsampling=%d, minangle=%f --- phi=%f,dphi=%f, dphi_sampling=%f, dphi_nsampling=%d, minphi=%f\n", dangle*double(iangle) + dangle/2., dangle, dangle_sampling, angle_sampling_steps,minangle,
//                         dphi*double(iangle) + dphi/2., dphi, dphi_sampling, phi_sampling_steps,minphi);
            
            double val=0;
            double norm=0;

            for(int i=0;i<angle_sampling_steps; i++)
                for(int j=0;j<phi_sampling_steps; j++){

                    double val=get_value_polar({
                                minangle+(double(i)+0.5)*dangle_sampling,
                                minphi + (double(j)+0.5)*dphi_sampling});
                    if(val>=0){
                        image[iangle][iphi]+=val;
                        norm+=1.;
                    }
                    
                }
            if(norm>0) image[iangle][iphi]/=norm;
            else image[iangle][iphi]=-1.;
                
            
            
            
        }
    }
    
    return image;  
    
}


// // // // // // // // // // // // // // // dd


std::vector<std::vector<double>> Pattern::get_cartesian_image(double* data_polar, std::array<double,2> angle_limits, std::array<double,2> phi_limits,  int nangles, int nphi){
    
    std::vector<std::vector<double>> image(this->dims[1], std::vector<double>(this->dims[0],0));
    
    double dangle=(angle_limits[1]-angle_limits[0])/double(nangles);
    double dphi=(phi_limits[1]-phi_limits[0])/double(nphi);
    

    
    #pragma omp parallel for schedule(dynamic)
    for(std::size_t y = 0; y<this->dims[1]; y++)
        for(std::size_t x = 0; x<this->dims[0]; x++){
            double xrel=double(x)-this->center[0];
            double yrel=double(y)-this->center[1];
            
            double angle = std::sqrt(xrel*xrel+yrel*yrel)*this->dtheta;
            double phi=std::atan2(yrel,xrel)/M_PI*180.;
            while(phi+360. <= phi_limits[1]){ phi+=360.;}
            
            
            /*if(phi<=phi_limits[1] && phi>=phi_limits[0] && angle<=angle_limits[1] && angle>=angle_limits[0])*/
            if(1){
                double phi_pixel=(phi-phi_limits[0])/dphi-0.5;
                double angle_pixel=(angle-angle_limits[0])/dangle-0.5;
                
                int iphi=std::floor(phi_pixel);
                int iangle=std::floor(angle_pixel);
                
                double fracphi=phi_pixel-std::floor(phi_pixel);
                double fracangle=angle_pixel-std::floor(angle_pixel);
                
                double outval=0;
                double norm=0;
                
                if(iphi>0 && iangle>0 &&  iphi<nphi && iangle<nangles){
                    double val=data_polar[iphi+iangle*nangles];
                    if(val>=0){
                        double factor=(1.-fracangle)*(1.-fracphi);
                        outval+=val*factor;
                        norm+=factor;
                    }
                }
                if(iphi+1>0 && iangle>0 && iphi+1<nphi && iangle<nangles){
                    double val=data_polar[iphi+1+iangle*nangles];
                    if(val>=0){
                        double factor=(1.-fracangle)*(fracphi);
                        outval+=val*factor;
                        norm+=factor;
                    }
                }               
                if(iphi>0 && iangle+1>0 && iphi<nphi && iangle+1<nangles){
                    double val=data_polar[iphi+(iangle+1)*nangles];
                    if(val>=0){
                        double factor=(fracangle)*(1.-fracphi);
                        outval+=val*factor;
                        norm+=factor;
                    }
                }
                if(iphi+1>0 && iangle+1>0 && iphi+1<nphi && iangle+1<nangles){
                    double val=data_polar[iphi+1+(iangle+1)*nangles];
                    if(val>=0){
                        double factor=(fracangle)*(fracphi);
                        outval+=val*factor;
                        norm+=factor;
                    }
                }
                
                
                if(norm>0){
                    image[y][x]=outval/norm;
                }
                else{
                    image[y][x]=-1;
                }
                

            }
            
        }
    
    return image;  
    
}

// // // // // // // // // // // 


std::tuple<std::vector<double>, std::vector<double>> Pattern::get_radial_distribution(std::array<double,2> angles_limit, std::array<double,2> phi_limits){
  
    std::vector<double> angles;
    std::vector<double> intensity;
    
    
    for(std::size_t y = 0; y<this->dims[1]; y++)
        for(std::size_t x = 0; x<this->dims[0]; x++){
            double xrel=(x-this->center[0]);
            double yrel=(y-this->center[1]);
            
            double val=this->get_data({x,y});
            
            double angle=std::atan2(yrel, xrel)/M_PI*180.;
            while(angle+360. <= phi_limits[1]){ angle+=360.;}
            
            if(angle<=phi_limits[1] && angle>=phi_limits[0] && val>=0){
                double radius = std::sqrt(xrel*xrel + yrel*yrel)*this->dtheta;
                
                if(radius <= angles_limit[1] && radius >=angles_limit[0]){
                    angles.push_back(radius);
                    intensity.push_back(val);
                }
            }
            
            
        }
        
    return std::make_tuple(angles, intensity);
        
}






// // // // // // // // // // // // // // // // // // // // // // 




std::vector<double> Pattern::get_angular_profile(std::vector<double> phi, double angle){
    
    std::vector<double> profile(phi.size(), 0);
    std::vector<double> norm(phi.size(), 0);

    for( std::size_t iphi=0; iphi<phi.size()-1; iphi++){
            double dphi=(phi[iphi+1]-phi[iphi])/double(INNER_STEPS);
            int limit = INNER_STEPS;
            if(iphi==phi.size()-1) limit++;
        
            
            for(int iin=0; iin<limit; iin++){
                double val=this->get_value_polar({angle, phi[iphi]+dphi*double(iin)});
                if(val>0){
                    double factor=1.-dphi*double(iin)/(phi[iphi+1]-phi[iphi]);
                
                    profile[iphi]+=factor*val;
                    norm[iphi]+=factor;
                    
                    profile[iphi+1]+=(1.-factor)*val;
                    norm[iphi+1]+=(1.-factor);
                    
                }
            }
    }
    
    
    for( std::size_t iphi=0; iphi<phi.size(); iphi++){
            if(norm[iphi]>0) profile[iphi]/=norm[iphi];
            else profile[iphi]=-1;
        }
    
    
    return profile;  
    
}



std::vector<double> Pattern::get_angular_profile(std::vector<double> phi, std::array<double,2>  angle_limits){
    
    int nangles = std::ceil((angle_limits[1]-angle_limits[0])/this->dtheta*2);
    
    if(nangles<2) nangles=2;
    double dangle=(angle_limits[1]-angle_limits[0])/double(nangles-1);
    
    
    std::vector<double> profile(phi.size(), 0);
    std::vector<double> counts(phi.size(), 0);

    
    for( int iangle=0; iangle<nangles; iangle++){
        auto temp_profile= this->get_angular_profile(phi, angle_limits[0]+double(iangle)*dangle);
        
        for( std::size_t iphi=0; iphi<phi.size(); iphi++)
            if(temp_profile[iphi]>=0){
                profile[iphi]+=temp_profile[iphi];
                counts[iphi]+=1.;
            }
    }
    
    for( std::size_t iphi=0; iphi<phi.size(); iphi++){
        if(counts[iphi]>0){
            profile[iphi]/=counts[iphi];
        }
        else profile[iphi]=-1;
    }
    
    return profile;
}


double Pattern::get_circularity(std::vector<double> phi, double angle){
    
    auto angprof=this->get_angular_profile(phi, angle);
    
    
    
    double sum=0;
    double ssum=0;
    double counts=0;
    for(auto val : angprof)if(val>0){
        sum+=val;
        ssum+=val*val;
        counts+=1;}
        
    
    if(counts>3){
                double stddev = std::sqrt(std::abs(ssum/counts - (sum/counts)*(sum/counts)));
                double aver = sum/counts;
                return std::max(1.-stddev/aver,0.);
        
    }
    else return -1;
    
    
    
    
}

double Pattern::get_circularity(double angle){
    
    
    std::vector<double> phi(100,0);
    
    double dphi=360./double(phi.size());
    
    for(std::size_t i=0; i<phi.size(); i++){
        phi[i]= dphi*double(i);
    }
    
    return this->get_circularity(phi, angle);
}


// // // // // // // // // // // // // // d

double Pattern::get_intensity(){
    
    double sum=0;
    for(std::size_t i=0; i<this->dims[0]*this->dims[1]; i++){
        if(this->data[i]>=0)  sum+=this->data[i];
    }
    return sum;
}


// double Pattern::get_center_value(std::vector<double>& angle_vec, std::vector<double>& phi_vec){
//     
//         double dev=0;
//         std::vector<std::vector<double>> radprofs(phi_vec.size());
// //         double totsum=0;
//         
//         for(int iphi=0; iphi<phi_vec.size(); iphi++){
//             radprofs[iphi]=this->get_radial_profile(angle_vec, phi_vec[iphi]);            
//         }
//         
//             
//         for(int iphi=0; iphi<phi_vec.size()-1; iphi++)
//           for(int iphi2=iphi+1; iphi2<phi_vec.size(); iphi2++){
//             double sum=0;
//             double ssum=0;
//             double counts=0;
//             
//             for(int iang=0; iang<angle_vec.size(); iang++)
//                 if(radprofs[iphi][iang] >  0 && radprofs[iphi2][iang]>0) {
//                                 double val=std::log(radprofs[iphi][iang]/radprofs[iphi2][iang]);
//                                 sum+=val;
//                                 ssum+=val*val;
//                                 counts+=1.;
//                 }
//                 
//             
//             
//             if(counts>2 && sum>0){
//                 double stddev = std::sqrt(ssum/counts - (sum/counts)*(sum/counts));
// //                 double aver = sum/counts;
// //                 dev+=stddev/aver;
//                 dev+=stddev;
//             }
//         }
//         
//     return dev;
//     
// }



// double Pattern::get_center_value(std::vector<double>& angle_vec, std::vector<double>& phi_vec){
//     
//     double val=0;
//     double counts=0;
//     for(int iang=0; iang<angle_vec.size(); iang++){
//         double circ_val=get_circularity(phi_vec, angle_vec[iang]);
//         if(circ_val>=0) {
//             val+=1-circ_val;
//             counts+=1;
//         }
//     }
//     
//     if(counts>0) return val/counts;
//     else return -1;
//                 
// }


/*
double Pattern::get_angular_correlation(std::vector<double> phi, double angle){
    
    auto angprof=this->get_angular_profile(phi, angle);
    
    std::vector<double> angprof_original(angprof);

    
    double sum=0;

    double counts=0;
    for(auto& val : angprof)if(val>=0){
        sum+=val;
        counts+=1;}
    double aver=sum/counts;
    
    
    for(auto& val : angprof)if(val>=0) val= (val-aver)/aver;
    
    
    std::vector<double> corr(phi.size()/2, 0);
    
    counts=0;
    
    for(int icorr=1; icorr<corr.size()+1; icorr++){
        counts=0;
        for(int iang=0; iang<angprof.size()-icorr; iang++) 
          if(angprof_original[iang]>=0 && angprof_original[iang+1]>=0){
            corr[icorr-1]+=angprof[iang]*angprof[iang+icorr];
            counts+=1;
            }
            
        if(counts>1) corr[icorr-1]=std::abs(corr[icorr-1]/counts);
        else corr[icorr-1]=-1;
    }
        
    
    double corrsum=0;
    counts=0;
    
    for(auto val : corr) if(val>=0){corrsum+=val; counts+=1; }
    
    if(counts>1) return std::abs(corrsum/counts);
    else return -1;
       
    
}*/


double Pattern::get_angular_correlation(std::vector<double> phi, double angle){
    
    auto angprof=this->get_angular_profile(phi, angle);
    
    std::vector<double> angprof_original(angprof);

    
    double sum=0;

    double counts=0;
    for(auto& val : angprof)if(val>=0){
        sum+=val;
        counts+=1;}
    double aver=sum/counts;
    
    
    for(auto& val : angprof)if(val>=0) val= (val-aver)/aver;
    
    
    std::vector<double> corr(phi.size()/2, 0);
    
    counts=0;
    
    for(int icorr=1; icorr<corr.size()+1; icorr++){
        counts=0;
        for(int iang=0; iang<angprof.size()-icorr; iang++) 
          if(angprof_original[iang]>=0 && angprof_original[iang+1]>=0){
            corr[icorr-1]+=std::abs(angprof[iang]-angprof[iang+icorr]);
            counts+=1;
            }
            
        if(counts>1) corr[icorr-1]=std::abs(corr[icorr-1]/counts);
        else corr[icorr-1]=-1;
    }
        
    
    double corrsum=0;
    counts=0;
    
    for(auto val : corr) if(val>=0){corrsum+=val; counts+=1; }
    
    if(counts>1) return std::abs(corrsum/counts);
    else return -1;
       
    
}
/*
double Pattern::get_center_value(std::vector<double>& radii_vec, std::vector<double>& phi_vec){
    
    this->dtheta=1;
    
    double val=0;
    double norm=0;

    double counts=0;
    
    double minaver=0;
    double maxaver=0;
    
    
    for(int iang=0; iang<radii_vec.size(); iang++){

        auto angprof=this->get_angular_profile(phi_vec, radii_vec[iang]);
        int span=angprof.size()/8;
//         int span=8;
        
        std::sort(angprof.begin(), angprof.end());
        
        int startindex=0;
        for(auto val: angprof){
            if(val>=0) break;
            startindex++;
        }
        
        int vecsize=angprof.size()-startindex;
        
        if(vecsize/2<span) span=vecsize/2;
        
        if(span>0){
            for(int i=0; i<span; i++){
                minaver+=angprof[i+startindex];
                maxaver+=angprof[angprof.size() -1 -i];
            }
            minaver/=double(span);
            maxaver/=double(span);
//             printf("%f, %f\n", maxaver, minaver);
            double average,stddev;
            get_stddev(angprof, average, stddev);
            
            val+=(maxaver-minaver)/average;
            norm+=average;
            counts+=1;
            }
        
    }
    
//     if(counts>0) return val/norm;
    if(counts>0) return val/counts;
    else return -1;
                
}*/

/*
double Pattern::get_center_value(std::vector<double>& radii_vec, std::vector<double>& phi_vec){
    
    this->dtheta=1;
    
    double val=0;
    double norm=0;

    double counts=0;
    
    for(int iang=0; iang<radii_vec.size(); iang++){
        auto angprof=this->get_angular_profile(phi_vec, radii_vec[iang]);
        double average;
        double stddev;
        get_stddev(angprof, average, stddev);
        double absdev;
        get_absdev(angprof, average, absdev);
        
        if(average>=0){       
//             val+=(stddev/average)*(stddev/average);

//             val+=stddev/average;
            val+=absdev/average;
//             norm+=average;
            counts+=1;
        }
    }
    
//     if(counts>0) return val/norm;
    if(counts>0) return val/counts;
    else return -1;
                
}*/


double Pattern::get_center_value(std::vector<double>& radii_vec, std::vector<double>& phi_vec){
    
    this->dtheta=1;
    
    double val=0;
    double counts=0;
    for(int iang=0; iang<radii_vec.size(); iang++){
        double circ_val=get_angular_correlation(phi_vec, radii_vec[iang]);
        if(circ_val>=0) {
            val+=circ_val;
            counts+=1;
        }
    }
    
    if(counts>0) return val/counts;
    else return -1;
                
}


// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // dd







std::vector<double> Pattern::flat_correction(double R, double dx, bool intcorrflag){
        std::vector<double> outdata(this->dims[0]*this->dims[1],0);
        std::vector<double> tempweight(this->dims[0]*this->dims[1],0);
        
        
        double mindist=this->center[0];
        if(this->center[1]<mindist) mindist=this->center[1];
        if((this->dims[0] - this->center[0])<mindist) mindist=(this->dims[0] - this->center[0]);
        if((this->dims[1] - this->center[1])<mindist) mindist=(this->dims[1] - this->center[1]);
        
        double min_theta=std::atan2(dx*mindist,R);
        
        double out_dtheta=min_theta/double(mindist);
        
        
        
        
        for(std::size_t y = 0; y<this->dims[1]; y++)
          for(std::size_t x = 0; x<this->dims[0]; x++){
              std::size_t index= x+y*this->dims[0];
              if(this->data[index]>=0){
                double ix = double(x) - this->center[0];
                double iy = double(y) - this->center[1];
                
                double flat_radius = std::sqrt(ix*ix+iy*iy)*dx;
                
                double local_theta=std::atan2(flat_radius,R);
                                
                double curved_radius = local_theta/out_dtheta;
                
                double phi = std::atan2(iy,ix);
                
                
                double xt = curved_radius * std::cos(phi);
                double yt = curved_radius * std::sin(phi);  
                

                
                xt += (double) this->center[0];
                yt += (double) this->center[1];
                
                int ixt = std::floor(xt);
                int iyt = std::floor(yt);

                double xtdec = xt-double(ixt);
                double ytdec = yt-double(iyt);
                
                double int_corr=1.;
                if(intcorrflag==1) int_corr = 1./std::pow(std::cos(local_theta), 3);
                
                double totweight=0;
                double val = this->data[index]*int_corr;
                
                    if(ixt>=0 && iyt>=0 && ixt<this->dims[0] && iyt<this->dims[1] ){
                        double weight = (1.-xtdec)*(1.-ytdec);
                        outdata[ixt+iyt*this->dims[0]]+= val*weight;
                        tempweight[ixt+iyt*this->dims[0]]+= weight;
                        
                    }
                    if(ixt+1>=0 && iyt>=0 && ixt+1<this->dims[0] && iyt<this->dims[1] ){
                        double weight = (xtdec)*(1.-ytdec);
                        outdata[ixt+1+iyt*this->dims[0]]+= val*weight;
                        tempweight[ixt+1+iyt*this->dims[0]]+= weight;
                    }
                    if(ixt>=0 && iyt+1>=0 && ixt<this->dims[0] && iyt+1<this->dims[1] ){
                        double weight = (1.-xtdec)*(ytdec);
                        outdata[ixt+(iyt+1)*this->dims[0]]+= val*weight;
                        tempweight[ixt+(iyt+1)*this->dims[0]]+= weight;
                    }
                    if(ixt+1>=0 && iyt+1>=0 && ixt+1<this->dims[0] && iyt+1<this->dims[1] ){
                        double weight = (xtdec)*(ytdec);
                        outdata[ixt+1+(iyt+1)*this->dims[0]]+= val*weight;
                        tempweight[ixt+1+(iyt+1)*this->dims[0]]+= weight;
                    }
              }
                
        }
        
        for(std::size_t i=0; i<outdata.size(); i++){
            if(tempweight[i]>3e-1) outdata[i]/=tempweight[i];
            else outdata[i]=-1;            
        }
        
        this->dtheta=out_dtheta/M_PI*180.;
        
        return outdata;
             
        
    }
    
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 
    
 

std::vector<double> Pattern::get_spikes_mask(double minval, double upfactor, double lowfactor){
        std::vector<double> outmask(this->dims[1]*this->dims[0],0);


        
        for(std::size_t y = 1; y<this->dims[1]-1; y++)
          for(std::size_t x = 1; x<this->dims[0]-1; x++){
              std::size_t index= x+y*this->dims[0];
              if(this->data[index]>=minval){
                  double tot=0;
                  double sum=0;
                  
                  for(int ly=y-1; ly<=y+1;ly++)
                      for(int lx=x-1; lx<=x+1;lx++){
                          std::size_t lindex= lx+ly*this->dims[0];
                          if(!(lindex==index)){
                              if(this->data[lindex]>=0){
                                  tot+=1.;
                                  sum+=this->data[lindex];
                            }
                              
                                  
                          }
                          
                   if(tot>2){
                        double avval = sum/tot;
                        if(this->data[index]>avval*upfactor || this->data[index]<avval*lowfactor)
                            outmask[index]=1;
                       
                    }
                          
                    }


              }
                
        }
        
                
        return outmask;
             
        
    }
    
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

std::vector<double> Pattern::downscale(std::array<std::size_t,2> newdims){
        std::vector<double> outdata(newdims[0]*newdims[1],0);
        std::vector<double> tempweight(newdims[0]*newdims[1],0);
        std::vector<double> totweight(newdims[0]*newdims[1],0);
        
//         std::array<double,2> scalefactor({double(this->dims[0])/double(newdims[0]), double(this->dims[1])/double(newdims[1])}); 
        
        double scalefactor[2]={double(this->dims[0])/double(newdims[0]), double(this->dims[1])/double(newdims[1])}; 
        
        
        for(std::size_t y = 0; y<this->dims[1]; y++)
          for(std::size_t x = 0; x<this->dims[0]; x++){
              std::size_t index= x+y*this->dims[0];
//               if(this->data[index]>=0){
                double ix = double(x)/scalefactor[0];
                double iy = double(y)/scalefactor[0];
                
                int ixt = std::floor(ix);
                int iyt = std::floor(iy);

                double xtdec = ix-double(ixt);
                double ytdec = iy-double(iyt);
                
                
                double val = this->data[index];
//                 if(val>=0){
                
                    if(ixt>=0 && iyt>=0 && ixt<newdims[0] && iyt<newdims[1] ){
                        double weight = (1.-xtdec)*(1.-ytdec);
                        if(val>=0) outdata[ixt+iyt*newdims[0]]+= val*weight;
                        if(val>=0) tempweight[ixt+iyt*newdims[0]]+= weight;
                        totweight[ixt+(iyt)*newdims[0]]+= weight;                        
                    }
                    if(ixt+1>=0 && iyt>=0 && ixt+1<newdims[0] && iyt<newdims[1] ){
                        double weight = (xtdec)*(1.-ytdec);
                        if(val>=0) outdata[ixt+1+iyt*newdims[0]]+= val*weight;
                        if(val>=0) tempweight[ixt+1+iyt*newdims[0]]+= weight;
                        totweight[ixt+1+(iyt)*newdims[0]]+= weight;                        

                    }
                    if(ixt>=0 && iyt+1>=0 && ixt<newdims[0] && iyt+1<newdims[1] ){
                        double weight = (1.-xtdec)*(ytdec);
                        if(val>=0) outdata[ixt+(iyt+1)*newdims[0]]+= val*weight;
                        if(val>=0) tempweight[ixt+(iyt+1)*newdims[0]]+= weight;
                        totweight[ixt+(iyt+1)*newdims[0]]+= weight;                        
                        
                    }
                    if(ixt+1>=0 && iyt+1>=0 && ixt+1<newdims[0] && iyt+1<newdims[1] ){
                        double weight = (xtdec)*(ytdec);
                        if(val>=0) outdata[ixt+1+(iyt+1)*newdims[0]]+= val*weight;
                        if(val>=0) tempweight[ixt+1+(iyt+1)*newdims[0]]+= weight;
                        totweight[ixt+1+(iyt+1)*newdims[0]]+= weight;
                    }
//                 }
//               }
                
        }
        
//         double maxweight = 0;
//         for(std::size_t i=0; i<outdata.size(); i++) if(tempweight[i]>maxweight) maxweight = tempweight[i];
        
        
        for(std::size_t i=0; i<outdata.size(); i++){
            if(tempweight[i]>WFRAC*totweight[i]) outdata[i]/=tempweight[i];
            else outdata[i]=-1;            
        }
                
        return outdata;
             
        
    }
    
    

std::vector<double> Pattern::downscale_nointerp(std::array<std::size_t,2> newdims){
        std::vector<double> outdata(newdims[0]*newdims[1],0);
        std::vector<double> tempweight(newdims[0]*newdims[1],0);
        std::vector<double> totweight(newdims[0]*newdims[1],0);
        
//         std::array<double,2> scalefactor({double(this->dims[0])/double(newdims[0]), double(this->dims[1])/double(newdims[1])}); 
        
        double scalefactor[2]={double(this->dims[0])/double(newdims[0]), double(this->dims[1])/double(newdims[1])}; 
        
        
        for(std::size_t y = 0; y<this->dims[1]; y++)
          for(std::size_t x = 0; x<this->dims[0]; x++){
              std::size_t index= x+y*this->dims[0];
//               if(this->data[index]>=0){
                double ix = double(x)/scalefactor[0];
                double iy = double(y)/scalefactor[0];
                
                int ixt = std::floor(ix);
                int iyt = std::floor(iy);

                double xtdec = ix-double(ixt);
                double ytdec = iy-double(iyt);
                
                
                double val = this->data[index];
//                 if(val>=0){
                
                if(ixt>=0 && iyt>=0 && ixt<newdims[0] && iyt<newdims[1] ){
                    double weight = 1.;
                    if(val>=0) outdata[ixt+iyt*newdims[0]]+= val*weight;
                    if(val>=0) tempweight[ixt+iyt*newdims[0]]+= weight;
                    totweight[ixt+(iyt)*newdims[0]]+= weight;                        
                }

//                 }
//               }
                
        }
        
//         double maxweight = 0;
//         for(std::size_t i=0; i<outdata.size(); i++) if(tempweight[i]>maxweight) maxweight = tempweight[i];
        
        
        for(std::size_t i=0; i<outdata.size(); i++){
            if(tempweight[i]>WFRAC*totweight[i]) outdata[i]/=tempweight[i];
            else outdata[i]=-1;            
        }
                
        return outdata;
             
        
    }
    

/*
std::vector<double> Pattern::downscale(std::array<std::size_t,2> newdims){
        std::vector<double> outdata(newdims[0]*newdims[1],0);
        std::vector<double> tempweight(newdims[0]*newdims[1],0);
        std::vector<double> totweight(newdims[0]*newdims[1],0);
        
//         std::array<double,2> scalefactor({double(this->dims[0])/double(newdims[0]), double(this->dims[1])/double(newdims[1])}); 
        
        double scalefactor[2]={double(this->dims[0])/double(newdims[0]), double(this->dims[1])/double(newdims[1])}; 
        
        
        for(std::size_t y = 0; y<this->dims[1]; y++)
          for(std::size_t x = 0; x<this->dims[0]; x++){
              std::size_t index= x+y*this->dims[0];
              if(this->data[index]>=0){
                double ix = double(x)/scalefactor[0];
                double iy = double(y)/scalefactor[0];
                
                int ixt = std::floor(ix);
                int iyt = std::floor(iy);

                double xtdec = ix-double(ixt);
                double ytdec = iy-double(iyt);
                
                
                double val = this->data[index];
//                 if(val>=0){
                
                    if(ixt>=0 && iyt>=0 && ixt<newdims[0] && iyt<newdims[1] ){
                        double weight = (1.-xtdec)*(1.-ytdec);
                        if(val>=0) outdata[ixt+iyt*newdims[0]]+= val*weight;
                        if(val>=0) tempweight[ixt+iyt*newdims[0]]+= weight;
                        totweight[ixt+(iyt)*newdims[0]]+= weight;                        
                    }
                    if(ixt+1>=0 && iyt>=0 && ixt+1<newdims[0] && iyt<newdims[1] ){
                        double weight = (xtdec)*(1.-ytdec);
                        if(val>=0) outdata[ixt+1+iyt*newdims[0]]+= val*weight;
                        if(val>=0) tempweight[ixt+1+iyt*newdims[0]]+= weight;
                        totweight[ixt+1+(iyt)*newdims[0]]+= weight;                        

                    }
                    if(ixt>=0 && iyt+1>=0 && ixt<newdims[0] && iyt+1<newdims[1] ){
                        double weight = (1.-xtdec)*(ytdec);
                        if(val>=0) outdata[ixt+(iyt+1)*newdims[0]]+= val*weight;
                        if(val>=0) tempweight[ixt+(iyt+1)*newdims[0]]+= weight;
                        totweight[ixt+(iyt+1)*newdims[0]]+= weight;                        
                        
                    }
                    if(ixt+1>=0 && iyt+1>=0 && ixt+1<newdims[0] && iyt+1<newdims[1] ){
                        double weight = (xtdec)*(ytdec);
                        if(val>=0) outdata[ixt+1+(iyt+1)*newdims[0]]+= val*weight;
                        if(val>=0) tempweight[ixt+1+(iyt+1)*newdims[0]]+= weight;
                        totweight[ixt+1+(iyt+1)*newdims[0]]+= weight;
                    }
//                 }
              }
                
        }
        
//         double maxweight = 0;
//         for(std::size_t i=0; i<outdata.size(); i++) if(tempweight[i]>maxweight) maxweight = tempweight[i];
        
        
        for(std::size_t i=0; i<outdata.size(); i++){
            if(tempweight[i]>WFRAC*totweight[i]) outdata[i]/=tempweight[i];
            else outdata[i]=-1;            
        }
                
        return outdata;
             
        
    }*/

// std::vector<double> Pattern::downscale(std::array<std::size_t,2> newdims){
//         std::vector<double> outdata(newdims[0]*newdims[1],0);
//         std::vector<double> tempweight(newdims[0]*newdims[1],0);
//         
// //         std::array<double,2> scalefactor({double(this->dims[0])/double(newdims[0]), double(this->dims[1])/double(newdims[1])}); 
//         
//         double scalefactor[2]={double(this->dims[0])/double(newdims[0]), double(this->dims[1])/double(newdims[1])}; 
//         
//         
//         for(std::size_t y = 0; y<this->dims[1]; y++)
//           for(std::size_t x = 0; x<this->dims[0]; x++){
//               std::size_t index= x+y*this->dims[0];
//               if(this->data[index]>=0){
//                 double ix = double(x)/scalefactor[0];
//                 double iy = double(y)/scalefactor[0];
//                 
//                 int ixt = std::floor(ix);
//                 int iyt = std::floor(iy);
// 
//                 double xtdec = ix-double(ixt);
//                 double ytdec = iy-double(iyt);
//                 
//                 
//                 double totweight=0;
//                 double val = this->data[index];
//                 if(val>=0){
//                 
//                     if(ixt>=0 && iyt>=0 && ixt<newdims[0] && iyt<newdims[1] ){
//                         double weight = (1.-xtdec)*(1.-ytdec);
//                         outdata[ixt+iyt*newdims[0]]+= val*weight;
//                         tempweight[ixt+iyt*newdims[0]]+= weight;
//                         
//                     }
//                     if(ixt+1>=0 && iyt>=0 && ixt+1<newdims[0] && iyt<newdims[1] ){
//                         double weight = (xtdec)*(1.-ytdec);
//                         outdata[ixt+1+iyt*newdims[0]]+= val*weight;
//                         tempweight[ixt+1+iyt*newdims[0]]+= weight;
//                     }
//                     if(ixt>=0 && iyt+1>=0 && ixt<newdims[0] && iyt+1<newdims[1] ){
//                         double weight = (1.-xtdec)*(ytdec);
//                         outdata[ixt+(iyt+1)*newdims[0]]+= val*weight;
//                         tempweight[ixt+(iyt+1)*newdims[0]]+= weight;
//                     }
//                     if(ixt+1>=0 && iyt+1>=0 && ixt+1<newdims[0] && iyt+1<newdims[1] ){
//                         double weight = (xtdec)*(ytdec);
//                         outdata[ixt+1+(iyt+1)*newdims[0]]+= val*weight;
//                         tempweight[ixt+1+(iyt+1)*newdims[0]]+= weight;
//                     }
//                 }
//               }
//                 
//         }
//         
//         for(std::size_t i=0; i<outdata.size(); i++){
//             if(tempweight[i]>3e-1) outdata[i]/=tempweight[i];
//             else outdata[i]=-1;            
//         }
//                 
//         return outdata;
//              
//         
//     }

    

std::vector<double> Pattern::downscale_and_correct(double R, double dx, std::array<std::size_t,2> newdims, std::vector<double> mask){
        std::vector<double> outdata(newdims[0]*newdims[1],0);
        std::vector<double> tempweight(newdims[0]*newdims[1],0);
        
//         std::array<double,2> scalefactor({double(this->dims[0])/double(newdims[0]), double(this->dims[1])/double(newdims[1])}); 
        double maxdist=this->center[0];
        if(this->center[1]>maxdist) maxdist=this->center[1];
        if((this->dims[0] - this->center[0])>maxdist) maxdist=(this->dims[0] - this->center[0]);
        if((this->dims[1] - this->center[1])>maxdist) maxdist=(this->dims[1] - this->center[1]);
        
        double max_theta=std::atan2(dx*maxdist,R);
        double r_max_new = R * 2. * std::sin(max_theta/2.);
        double dx_new = r_max_new/maxdist;

        double scalefactor[2]={double(this->dims[0])/double(newdims[0]), double(this->dims[1])/double(newdims[1])}; 
        
        
        for(std::size_t y = 0; y<this->dims[1]; y++)
          for(std::size_t x = 0; x<this->dims[0]; x++){
              std::size_t index= x+y*this->dims[0];
              if(mask[index]>0.5){
                
                double relx = double(x)- double(this->center[0]);
                double rely = double(y)- double(this->center[1]);
                  
                double phi = std::atan2(rely,relx);
                double r_orig = std::sqrt(relx*relx+rely*rely)*dx;
                double theta = std::atan2(r_orig, R);
                
                
//                 if(theta/M_PI * 180.>15)
//                     printf("%.2f\n", theta/M_PI * 180.);
                
                double r_new = R * 2. * std::sin(theta/2.);
                double r_new_pixels = r_new/dx_new;
                
                double x_new_rel = r_new_pixels*std::cos(phi);
                double y_new_rel = r_new_pixels*std::sin(phi);
                
                  
                  
                double ix = (x_new_rel + double(this->center[0]))/scalefactor[0];
                double iy = (y_new_rel + double(this->center[1]))/scalefactor[1];
                
                int ixt = std::floor(ix);
                int iyt = std::floor(iy);

                double xtdec = ix-double(ixt);
                double ytdec = iy-double(iyt);
                
                
                double totweight=0;
                double val = this->data[index];                
                val *= 1./std::pow(std::cos(theta), 3);
//                 if(theta/M_PI * 180.>15)
//                     printf("%.2f\n", 1./std::pow(std::cos(theta), 3));
                
                if(1){
                
                    if(ixt>=0 && iyt>=0 && ixt<newdims[0] && iyt<newdims[1] ){
                        double weight = (1.-xtdec)*(1.-ytdec);
                        outdata[ixt+iyt*newdims[0]]+= val*weight;
                        tempweight[ixt+iyt*newdims[0]]+= weight;
                        
                    }
                    if(ixt+1>=0 && iyt>=0 && ixt+1<newdims[0] && iyt<newdims[1] ){
                        double weight = (xtdec)*(1.-ytdec);
                        outdata[ixt+1+iyt*newdims[0]]+= val*weight;
                        tempweight[ixt+1+iyt*newdims[0]]+= weight;
                    }
                    if(ixt>=0 && iyt+1>=0 && ixt<newdims[0] && iyt+1<newdims[1] ){
                        double weight = (1.-xtdec)*(ytdec);
                        outdata[ixt+(iyt+1)*newdims[0]]+= val*weight;
                        tempweight[ixt+(iyt+1)*newdims[0]]+= weight;
                    }
                    if(ixt+1>=0 && iyt+1>=0 && ixt+1<newdims[0] && iyt+1<newdims[1] ){
                        double weight = (xtdec)*(ytdec);
                        outdata[ixt+1+(iyt+1)*newdims[0]]+= val*weight;
                        tempweight[ixt+1+(iyt+1)*newdims[0]]+= weight;
                    }
                }
              }
                
        }
        
        for(std::size_t i=0; i<outdata.size(); i++){
            if(tempweight[i]>=3e-1) outdata[i]/=tempweight[i];
            else outdata[i]=0;            
        }
                
        return outdata;
             
        
    }
    
    
    
    
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 


std::vector<std::vector<double>> Pattern::get_radial_profile_centers(std::vector<double> angles, std::array<double,2> phi_limits, std::vector<std::array<double,2>> centers){
    
    std::vector<std::vector<double>> profiles(centers.size(), std::vector<double>(angles.size(),0));
    std::vector<std::vector<double>> norms(centers.size(), std::vector<double>(angles.size(),0));
    
    int tries=500;
    
    #pragma omp parallel for schedule(dynamic)
    for(int iangle=0; iangle<angles.size(); iangle++){
        std::default_random_engine generator;
        std::uniform_real_distribution<double> distribution(phi_limits[0]/180.*M_PI,phi_limits[1]/180.*M_PI);
        
        for(int itry=0; itry<tries; itry++){
            double locphi=distribution(generator);
            
            double relx=angles[iangle] * std::cos(locphi)/this->dtheta;
            double rely=angles[iangle] * std::sin(locphi)/this->dtheta;
            
            for(std::size_t icenter=0; icenter<centers.size(); icenter++){
            
                double val=get_value({ relx + centers[icenter][0], rely + centers[icenter][1]});
            
                if(val>=0){
                    profiles[icenter][iangle] += val;
                    norms[icenter][iangle] += 1;
                 }
                        
                        
            }

        }
            
    }
        
        
    for(std::size_t icenter=0; icenter<centers.size(); icenter++){
        for( std::size_t iang=0; iang<angles.size(); iang++){
            if(norms[icenter][iang]>0) profiles[icenter][iang]/=norms[icenter][iang];
            else profiles[icenter][iang]=-1;
        }
    }
    return profiles;  
    
}




