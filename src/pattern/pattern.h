#pragma once

#include <vector>
#include <array>
#include <tuple>


class Pattern{
public:
    Pattern(double* data_in, std::array<std::size_t,2> dims_in, std::array<double,2> center_in, double dtheta_in):
            data(data_in), dims(dims_in), center(center_in), dtheta(dtheta_in){       
//             printf("%d,%d\n\n", dims[0], dims[1]);
//             printf("%f,%f\n\n", center[0], center[1]);

    };
    
    
    std::vector<double> flat_correction(double R, double dx, bool intcorrflag);
    
    std::vector<double> get_radial_profile(std::vector<double> angles, double phi);
    std::vector<double> get_radial_profile(std::vector<double> angles, std::array<double,2>  phi_limits);
    std::vector<double> get_radial_maxima(std::vector<double> angles, std::array<double,2>  phi_limits);

    std::vector<std::vector<double>> get_radial_profile_centers(std::vector<double> angles, std::array<double,2> phi_limits, std::vector<std::array<double,2>> centers);

    std::vector<double> get_angular_profile(std::vector<double> phi,  double angle);
    std::vector<double> get_angular_profile(std::vector<double> phi, std::array<double,2>  angle_limits);
    std::tuple<std::vector<double>, std::vector<double>> get_radial_distribution(std::array<double,2> angles, std::array<double,2> phi_limits);
    
    
    std::vector<std::vector<double>> get_polar_image(std::array<double,2> angle_limits, std::array<double,2> phi_limits,  int nangles, int nphi);
    std::vector<std::vector<double>> get_cartesian_image(double* data_polar, std::array<double,2> angle_limits, std::array<double,2> phi_limits,  int nangles, int nphi);

    std::vector<double> get_spikes_mask(double minval, double upfactor, double lowfactor);

    
    std::vector<double> downscale(std::array<std::size_t,2> newdims);
    std::vector<double> downscale_nointerp(std::array<std::size_t,2> newdims);

    std::vector<double> downscale_and_correct(double R, double dx, std::array<std::size_t,2> newdims, std::vector<double> mask);
//     std::array<double, 2> find_center(std::array<int, 2> xlim, std::array<int, 2> ylim, std::array<double, 2> angle, std::array<double, 2> phi);
//     std::array<int, 2> find_center_2(std::array<int, 2> xlim, std::array<int, 2> ylim, std::array<double, 2> angle, std::array<double, 2> phi);
    double get_center_value(std::vector<double>& angle_vec, std::vector<double>& phi_vec);
    
    double get_circularity(double angle);
    double get_circularity(std::vector<double> phi,  double angle);
    double get_intensity();
    double get_angular_correlation(std::vector<double> phi, double angle);

    
    double get_value(std::array<double,2> coords);
    double get_value_polar(std::array<double,2> coords);

    double get_data(std::array<std::size_t,2> coords);
    
    double* data;
    std::array<std::size_t,2>  dims;
    std::array<double,2> center;
    double dtheta;
    
    
};
