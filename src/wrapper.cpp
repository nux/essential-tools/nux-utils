#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>

#include <vector>
#include <array>
#include <cmath>
#include <string>
#include <variant>
#include <tuple>

#include "pattern/pattern.h"
#include "mie/core_shell.h"
#include <stdio.h>

namespace py = pybind11;



PYBIND11_MODULE(_nux_utils, nu)
{ 


    
    
    
    py::module pm = nu.def_submodule("pattern");
    
         pm.def("downscale", [](py::array_t<double> data_in,  std::array<std::size_t,2> newsize, bool interpolation){
                        Pattern pattern((double*) (std::size_t) data_in.request().ptr , std::array<std::size_t,2>{(std::size_t) data_in.request().shape[1], (std::size_t) data_in.request().shape[0]}, std::array<double,2>{0,0}, 1);
                               
                        if(interpolation==true){
                            auto result=pattern.downscale(newsize);
                            
                            auto nparray = py::array(result.size(), result.data());
                            nparray.resize({newsize[1],newsize[0]});
                            return nparray;
                        }
                        else{
                            auto result=pattern.downscale_nointerp(newsize);
                            
                            auto nparray = py::array(result.size(), result.data());
                            nparray.resize({newsize[1],newsize[0]});
                            return nparray;                            
                        }

                        },R"pbdoc(
                            Downscale the given pattern to the given size. Note that negative values in the input data are ignored.
                            
                            :param data: 2d numpy array, containing the diffraction image
                            :param newsize: 2-element array with the new size
                            
                            :returns: the rescaled diffraction image
                            )pbdoc", py::return_value_policy::move,  py::arg("data"), py::arg("newsize"), py::arg("interpolation")=true);
         
         pm.def("downscale_and_correct", [](py::array_t<double> data_in, py::array_t<double> mask_in, std::array<double,2> center_in, double R, double dx, std::array<std::size_t,2> newsize){
                        Pattern pattern((double*) (std::size_t) data_in.request().ptr , std::array<std::size_t,2>{(std::size_t) data_in.request().shape[1], (std::size_t) data_in.request().shape[0]}, center_in, 1);
                               
                        double* mask_array = (double*) mask_in.request().ptr;
                        std::vector<double> mask(data_in.request().shape[0]*data_in.request().shape[1]);
                        for(int i=0; i<data_in.request().shape[0]*data_in.request().shape[1];i++){
                            mask[i]=mask_array[i];
                        }
                        auto result=pattern.downscale_and_correct(R, dx,newsize,mask);
                        
                        auto nparray = py::array(result.size(), result.data());
                        nparray.resize({newsize[1],newsize[0]});
                        return nparray;

                        },R"pbdoc(
                            Downscale the given pattern to the given size. Note that negative values in the input data are ignored.
                            
                            :param data: 2d numpy array, containing the diffraction image
                            :param mask: 2d numpy array, containing the mask
                            :param center: 2-elements array containing the x and y coordinate of the pattern center
                            :param distance: detector distance
                            :param pixel_size: pixel size
                            :param newsize: 2-element array with the new size
                            
                            :returns: the rescaled diffraction image
                            )pbdoc", py::return_value_policy::move,  py::arg("data"), py::arg("mask"), py::arg("center"), py::arg("distance"), py::arg("pixel_size"),  py::arg("newsize"));
    
     pm.def("flat_correction", [](py::array_t<double> data_in,  std::array<double,2> center_in, double R, double dx, bool intcorrection=1){
                        Pattern pattern((double*) (std::size_t) data_in.request().ptr , std::array<std::size_t,2>{(std::size_t) data_in.request().shape[1], (std::size_t) data_in.request().shape[0]}, center_in, 1);
                               
                        auto result=pattern.flat_correction(R, dx, intcorrection);
                        
                        double* ptr= (double*) data_in.request().ptr;
                        for(int i=0; i<result.size(); i++)
                            ptr[i] = result[i];
                        return std::make_tuple(data_in, pattern.dtheta);

                        },R"pbdoc(
                            Corrects the effects of a flat detector
                            
                            :param data: 2d numpy array, containing the diffraction image
                            :param center: 2-elements array containing the x and y coordinate of the pattern center
                            :param distance: detector distance
                            :param pixel_size: pixel size
                            
                            :returns: a tuple, containing the corrected diffraction image and the dtheta value, which is then needed for the analysis
                            )pbdoc", py::return_value_policy::move,  py::arg("data"), py::arg("center"), py::arg("distance"), py::arg("pixel_size"), py::arg("intensity_correction")=1);

    pm.def("get_spikes_mask", [](py::array_t<double> data_in,  double minval, double upfactor, double lowfactor){
                        Pattern pattern((double*) (std::size_t) data_in.request().ptr , std::array<std::size_t,2>{(std::size_t) data_in.request().shape[1], (std::size_t) data_in.request().shape[0]}, std::array<double,2>{0,0}, 1);
                               
                        
                        auto result=pattern.get_spikes_mask(minval, upfactor, lowfactor);
                        
                        auto nparray = py::array(result.size(), result.data());
                        nparray.resize({(std::size_t) data_in.request().shape[1], (std::size_t) data_in.request().shape[0]});
                        return nparray;                            
                    

                        },R"pbdoc(
                            Get a mask for spikes in the diffraction pattern. Negative values are interpreted as masked values.
                            
                            :param data: 2d numpy array, containing the diffraction image
                            :param minval: minimum value for a spike
                            :param upfactor: spikes is identified if it has a value greater than upfactor times the average of the neighbours
                            
                            :returns: a mask matrix of the same size of the input pattern. Mask is 1 where spikes are identified, zero otherwise
                            )pbdoc", py::return_value_policy::move,  py::arg("data"), py::arg("minval"), py::arg("upfactor")=5, py::arg("lowfactor")=0.);
     
     
    pm.def("get_radial_profile", [](py::array_t<double> data_in, double dtheta_in, std::array<double,2> center_in, std::vector<double> angles_in, std::variant<double,std::array<double,2> > phi_in){
                        Pattern pattern((double*) (std::size_t) data_in.request().ptr , std::array<std::size_t,2>{(std::size_t) data_in.request().shape[1], (std::size_t) data_in.request().shape[0]}, center_in, dtheta_in );
                               
                        auto* phi  = std::get_if<double>(&phi_in);                        
                        if(phi!=nullptr) return pattern.get_radial_profile(angles_in, *phi);
                        else      return pattern.get_radial_profile(angles_in, std::get<std::array<double,2>>(phi_in));

                        }, R"pbdoc(
                            Get the radial profile  for a given set of angles. 
                            
                            The function computes the radial profile at the given scattering angles in a given scattering direction :math:`\phi`. 
                            
                            
                            :param data: 2d numpy array, containing the diffraction image
                            :param dtheta: angular extension of a single pixel. Usually computed as the ratio between the maximum scattering angle and half the pixel width of the detector
                            :param center: 2-elements array containing the x and y coordinate of the pattern center
                            :param angles: array of scattering angles, that must be sorted by increasing values. 
                            :param phi: scattering direction at which the radial profile is computed. If a list of 2 values is given, they represent the minimum and maximum scattering direction at which the radial profile is computed.
                            
                            :returns: a list of values corresponding to the radial profile.
                            )pbdoc",py::return_value_policy::move,  py::arg("data"), py::arg("dtheta"),py::arg("center"), py::arg("angles"), py::arg("phi"));
pm.def("get_radial_maxima", [](py::array_t<double> data_in, double dtheta_in, std::array<double,2> center_in, std::vector<double> angles_in, std::array<double,2> phi_in){
                        Pattern pattern((double*) (std::size_t) data_in.request().ptr , std::array<std::size_t,2>{(std::size_t) data_in.request().shape[1], (std::size_t) data_in.request().shape[0]}, center_in, dtheta_in );
                               
                        return pattern.get_radial_maxima(angles_in, phi_in);

                        }, R"pbdoc(
                            Get the radial maxima  for a given set of angles. 
                            
                            The function computes the radial profile at the given scattering angles in a given scattering direction :math:`\phi`. 
                            
                            
                            :param data: 2d numpy array, containing the diffraction image
                            :param dtheta: angular extension of a single pixel. Usually computed as the ratio between the maximum scattering angle and half the pixel width of the detector
                            :param center: 2-elements array containing the x and y coordinate of the pattern center
                            :param angles: array of scattering angles, that must be sorted by increasing values. 
                            :param phi: A list of 2 values is given, they represent the minimum and maximum scattering direction at which the radial profile is computed.
                            
                            :returns: a list of values corresponding to the radial profile.
                            )pbdoc",py::return_value_policy::move,  py::arg("data"), py::arg("dtheta"),py::arg("center"), py::arg("angles"), py::arg("phi"));
    
    
    
        pm.def("get_radial_distribution", [](py::array_t<double> data_in, double dtheta_in, std::array<double,2> center_in, std::array<double,2> angles_range, std::array<double,2> phi_in){
                        Pattern pattern((double*) (std::size_t) data_in.request().ptr , std::array<std::size_t,2>{(std::size_t) data_in.request().shape[1], (std::size_t) data_in.request().shape[0]}, center_in, dtheta_in );
                               
                        return pattern.get_radial_distribution(angles_range,phi_in );

                        }, R"pbdoc(
                            Get the radial intensity distribution of pixels. 
                                                        
                            
                            :param data: 2d numpy array, containing the diffraction image
                            :param dtheta: angular extension of a single pixel. Usually computed as the ratio between the maximum scattering angle and half the pixel width of the detector
                            :param center: 2-elements array containing the x and y coordinate of the pattern center
                            :param angles: 2 elements array that defines the minimum and maximum scattering angle.
                            :param phi:  2 elements array that represents the minimum and maximum scattering direction at which the radial profile is computed.
                            
                            :returns: a tuple of two arrays, one contains the angles, the other the intensity.
                            )pbdoc",py::return_value_policy::move,  py::arg("data"), py::arg("dtheta"),py::arg("center"), py::arg("angles"), py::arg("phi"));
    
    
    pm.def("get_angular_profile", [](py::array_t<double> data_in,  double dtheta_in, std::array<double,2> center_in, std::vector<double> phi_in, std::variant<double,std::array<double,2> >  angle_in){
                        Pattern pattern((double*) (std::size_t) data_in.request().ptr , std::array<std::size_t,2>{(std::size_t) data_in.request().shape[1], (std::size_t) data_in.request().shape[0]}, center_in, dtheta_in );
                                
                        auto* angle  = std::get_if<double>(&angle_in);                        
                        if(angle!=nullptr) return pattern.get_angular_profile(phi_in, *angle);
                        else      return pattern.get_angular_profile(phi_in, std::get<std::array<double,2>>(angle_in));
        
                        }, R"pbdoc(
                            Get the angular profile  for a given set of scattering directions. 
                            
                            The function computes the angular profile in the given scattering directions at a given scattering angle :math:`\theta`. 
                            
                            
                            :param data: 2d numpy array, containing the diffraction image
                            :param dtheta: angular extension of a single pixel. Usually computed as the ratio between the maximum scattering angle and half the pixel width of the detector
                            :param center: 2-elements array containing the x and y coordinate of the pattern center
                            :param phi: array of scattering directions, that must be sorted by increasing values. 
                            :param angle:  scattering direction  :math:`\theta` at which the angular profile is computed. If a list of 2 values is given, they represent the minimum and maximum scattering angles at which the angular profile is computed
                            
                            :returns: a list of values corresponding to the angular profile.
                        )pbdoc",py::return_value_policy::move,  py::arg("data"), py::arg("phi"), py::arg("dtheta"),py::arg("center"), py::arg("angle"));
    
    pm.def("get_circularity", [](py::array_t<double> data_in,  double dtheta_in, std::array<double,2> center_in,  std::vector<double> phi, double angle){
//                         auto data_2d=data_in.unchecked<2>();
                        Pattern pattern((double*) (std::size_t) data_in.request().ptr , std::array<std::size_t,2>{(std::size_t) data_in.request().shape[1], (std::size_t) data_in.request().shape[0]}, center_in, dtheta_in );
                        
                        return pattern.get_circularity(phi, angle);
        
                        }, R"pbdoc(
                            Provide a measure of the "circularity" of the diffraction pattern.
                            
                            The computation is performed by computing the standard deviation :math:`\sigma_v` of pixel values at a given scattering angle :math:`\theta`, normalized by the average value :math:`\overline{v}`. The returned circularity value is obtained by :math:`1-\frac{\sigma_v}{\overline{v}}`. The closer the value to 1, the higher the circularity.
                            
                            
                            :param data: 2d numpy array, containing the diffraction image
                            :param dtheta: angular extension of a single pixel. Usually computed as the ratio between the maximum scattering angle and half the pixel width of the detector
                            :param center: 2-elements array containing the x and y coordinate of the pattern center
                            :param phi: array of scattering directions at which the circularity is evaluated                            
                            :param angle: scattering angle at which the circularity value is computed
                            
                            :returns: the circularity value
                            )pbdoc", py::return_value_policy::move,  py::arg("data"), py::arg("dtheta"),py::arg("center"), py::arg("phi"), py::arg("angle"));
     
     
    
     
    pm.def("get_intensity", [](py::array_t<double> data_in){
                        Pattern pattern((double*) (std::size_t) data_in.request().ptr , std::array<std::size_t,2>{(std::size_t) data_in.request().shape[1], (std::size_t) data_in.request().shape[0]}, {0,0}, 1.);
        
                        return pattern.get_intensity();
        
                        },R"pbdoc(
                        Get the total intensity of a diffraction pattern.
        
                        This function returns the sum of all the pixel values of the diffraction pattern. Pixels with negative value are excluded.
        
                        :param data: 2d numpy array, containing the diffraction image
        
                        :returns: sum of all pixels (negative pixel values excluded)
                        )pbdoc", py::arg("data"));
    

     pm.def("get_center_value", [](py::array_t<double> data_in, std::array<double,2> center_in, std::vector<double>  radii, std::vector<double>  phi){
                        Pattern pattern((double*) (std::size_t) data_in.request().ptr , std::array<std::size_t,2>{(std::size_t) data_in.request().shape[1], (std::size_t) data_in.request().shape[0]}, center_in, 1 );
                        
                        return pattern.get_center_value(radii, phi);
        
                        }, py::return_value_policy::move,  py::arg("data"),  py::arg("center"),  py::arg("radii"), py::arg("phi"));  
     
     
     
     
     
         pm.def("get_radial_profile_centers", [](py::array_t<double> data_in, double dtheta_in, std::vector<std::array<double,2>> centers, std::vector<double> angles_in, std::array<double,2> phi_in){
                        Pattern pattern((double*) (std::size_t) data_in.request().ptr , std::array<std::size_t,2>{(std::size_t) data_in.request().shape[1], (std::size_t) data_in.request().shape[0]}, {0,0}, dtheta_in );
                        
                        return pattern.get_radial_profile_centers(angles_in, phi_in, centers);

                        }, py::return_value_policy::move,  py::arg("data"), py::arg("dtheta"),py::arg("centers"), py::arg("angles"), py::arg("phi"));
     
     
     
     
    pm.def("get_polar_image", [](py::array_t<double> data_in, double dtheta_in, std::array<double,2> center_in, std::array<double,2> angles_in, std::array<double,2> phi_in, std::array<double,2> size){
                        Pattern pattern((double*) (std::size_t) data_in.request().ptr , std::array<std::size_t,2>{(std::size_t) data_in.request().shape[1], (std::size_t) data_in.request().shape[0]}, center_in, dtheta_in );
                               
                        return pattern.get_polar_image(angles_in, phi_in, size[0], size[1]);

                        }, R"pbdoc(
                            Get the image in polar coordinates, theta (scatterin angle) and phi. 
                            
                            
                            
                            :param data: 2d numpy array, containing the diffraction image
                            :param dtheta: angular extension of a single pixel. Usually computed as the ratio between the maximum scattering angle and half the pixel width of the detector
                            :param center: 2-elements array containing the x and y coordinate of the pattern center
                            :param angles: 2-elements array containing the minimum and maximum scattering angle at which the image is computed 
                            :param phi: 2-elements array containing the minimum and maximum scattering direction at which the image is computed 
                            :param size: 2-elements array containing the size of the output image
                            
                            :returns: image in polar coordinates
                            )pbdoc",py::return_value_policy::move,  py::arg("data"), py::arg("dtheta"),py::arg("center"), py::arg("angles"), py::arg("phi")=std::array<double,2>{0,360}, py::arg("size")=std::array<double,2>{200,200});
     
     
     
     pm.def("get_cartesian_image", [](py::array_t<double> data_in, double dtheta_in, std::array<double,2> center_in, std::array<double,2> angles_in, std::array<double,2> phi_in, std::array<std::size_t,2> size){
                        double* foo;
                        Pattern pattern(foo ,size, center_in, dtheta_in );
                            
                        
                        return pattern.get_cartesian_image((double*) (std::size_t) data_in.request().ptr, angles_in, phi_in, (std::size_t) data_in.request().shape[1], (std::size_t) data_in.request().shape[0]);

                        }, R"pbdoc(
                            Get the image in polar coordinates, theta (scatterin angle) and phi. 
                            
                            
                            
                            :param data: 2d numpy array, containing the diffraction image in polar coordinates
                            :param dtheta: angular extension of a single pixel in cartesian coordinates.
                            :param center: 2-elements array containing the x and y cartesian coordinate of the pattern center
                            :param angles: 2-elements array containing the minimum and maximum scattering angle that the input polar image covers 
                            :param phi: 2-elements array containing the minimum and maximum scattering direction that the input polar image covers 
                            :param size: 2-elements array containing the size of the output cartesian image 
                            
                            :returns: image in polar coordinates
                            )pbdoc",py::return_value_policy::move,  py::arg("data"), py::arg("dtheta"),py::arg("center"), py::arg("angles"), py::arg("phi")=std::array<double,2>{0,360}, py::arg("size")=std::array<double,2>{200,200});    
     
     
     
     
// // // // // // // // // //     d
    py::module mm = nu.def_submodule("mie");
    

        mm.def("core_shell", [](double radius, double thickness, double delta_core, double beta_core, double delta_shell, double beta_shell, std::vector<double> angles){
            
                        
                        return mie::core_shell(
                                2.*M_PI*(radius-thickness), 1-delta_core, -beta_core,
                                2.*M_PI*radius, 1-delta_shell, -beta_shell,
                                angles);
        },
        R"pbdoc(
        Mie core shell simulation.
        
        This function simulates the radial profile of a spherical object made by a spherical core and a spherical shell. The radial profile is computed at the given angles.
        
        :param radius: radius of the whole sample.
        :param thickness: thickness of the shell.
        :param delta_core: :math:`\delta` value of the core
        :param beta_core: :math:`\beta` value of the core        
        :param delta_shell: :math:`\delta` value of the shell
        :param beta_shell: :math:`\beta` value of the shell    
        :param angles: scattering angles at which the radial scattering profile must be computed
        
        :returns: the scattering radial profile, resulting from the simulation with the given parameters.
           )pbdoc", py::arg("radius"), py::arg("thickness"), py::arg("delta_core"), py::arg("beta_core"), py::arg("delta_shell"), py::arg("beta_shell"), py::arg("angles") );
                            
      
        mm.def("sphere", [](double radius, double delta_core, double beta_core, std::vector<double> angles){
            
                        return mie::core_shell(
                                2.*M_PI*radius, 1-delta_core, -beta_core,
                                2.*M_PI*radius, 1-delta_core, -beta_core,
                                angles);
        },
        R"pbdoc(
        Mie core shell simulation.
        
        This function simulates the radial profile of a spherical sample. The radial profile is computed at the given angles.
        
        :param radius: radius of the whole sample.
        :param delta: :math:`\delta` value of the sample
        :param beta: :math:`\beta` value of the sample           
        :param angles: scattering angles at which the radial scattering profile must be computed
        
        :returns: the scattering radial profile, resulting from the simulation with the given parameters.
           )pbdoc", py::arg("radius"),  py::arg("delta"), py::arg("beta"), py::arg("angles") );
mm.def("sphere_efficiency", [](double radius, double delta_core, double beta_core){
            
                        return mie::core_shell_efficiency(
                                2.*M_PI*radius, 1-delta_core, -beta_core,
                                2.*M_PI*radius, 1-delta_core, -beta_core
                                );
        },
        R"pbdoc(
        Mie core shell simulation.
        
        This function computes the extinction, scattering and absorption efficiency.
        
        :param radius: radius of the whole sample.
        :param delta: :math:`\delta` value of the sample
        :param beta: :math:`\beta` value of the sample           

        
        :returns: A list containing the extinction, scattering and absorption efficiency.
           )pbdoc", py::arg("radius"),  py::arg("delta"), py::arg("beta"));      
        

    mm.def("sphere_pattern", [](double radius, double delta_core, double beta_core, double maxangle, int size, std::vector<double> center){
                        py::array_t<double> result = py::array_t<double>(size*size);
                        py::buffer_info buf = result.request();
                        double *ptr = (double *) buf.ptr;
                        
                        std::vector<double> localcenter(center);
                        if(localcenter[0]<0){
                            localcenter[0]=double(size)/2.;
                        }
                        if(localcenter[1]<0){
                            localcenter[1]=double(size)/2.;
                        }                 
                        
                        std::vector<double> angles(size*size);
                        double dtheta=2.*maxangle/(double(size));
                        for(int i=0; i<size*size;i++){
                            double x = double(i%size)-localcenter[0];
                            double y = double(i/size)-localcenter[1];
                            angles[i]=std::sqrt(x*x+y*y)*dtheta;
                        }
                        
                        
                        auto intensities=mie::core_shell(
                                2.*M_PI*radius, 1-delta_core, -beta_core,
                                2.*M_PI*radius, 1-delta_core, -beta_core,
                                angles);
                        for(int i=0; i<size*size;i++) ptr[i]=intensities[i];
               
                        result.resize({size,size});
                        return result;
                        
                        
        },
        R"pbdoc(
        Mie core shell simulation.
        
        This function simulates the radial profile of a spherical sample. The radial profile is computed at the given angles.
        
        :param radius: radius of the whole sample.
        :param delta: :math:`\delta` value of the sample
        :param beta: :math:`\beta` value of the sample           
        :param angle: maximum scattering angle
        :param resolution: pattern size, in pixels
        :param center: [x,y] coordinates of the pattern center. If negative, resolution/2 is assumed.
        
        
        :returns: the scattering image, resulting from the simulation with the given parameters.
           )pbdoc", py::arg("radius"),  py::arg("delta"), py::arg("beta"), py::arg("angle"), py::arg("resolution")=512, py::arg("center")=std::vector<double>({-1,-1}));
        
        mm.def("sphere_pattern_mask", [](double radius, double delta_core, double beta_core, double maxangle, py::array_t<int> mask, std::array<double,2> center){
                        py::buffer_info buf_mask = mask.request();
                        int *ptr_mask = (int *) buf_mask.ptr;
                        if(buf_mask.shape.size()!=2)
                                throw std::invalid_argument("Mask array must have 2 dimensions of the same size");
                        if(buf_mask.shape[0]!=buf_mask.shape[1])
                                throw std::invalid_argument("Mask array must have 2 dimensions of the same size");
                        int size=buf_mask.shape[0];
                        
                        
                        
                        py::array_t<double> result = py::array_t<double>(size*size);
                        py::buffer_info buf = result.request();
                        double *ptr = (double *) buf.ptr;
                        
                        std::vector<double> angles(size*size);
                        double dtheta=2.*maxangle/(double(size));
                        for(int i=0; i<size*size;i++){
                            if(ptr_mask[i]!=0){
                            double x = double(i%size-size/2.)+center[0];
                            double y = double(i/size-size/2.)+center[1];
                            angles[i]=std::sqrt(x*x+y*y)*dtheta;
                            }
                            else angles[i]=-1;
                        }
                        
                        
                        mie::core_shell(
                                2.*M_PI*radius, 1-delta_core, -beta_core,
                                2.*M_PI*radius, 1-delta_core, -beta_core,
                                angles, ptr);
               
                        result.resize({size,size});
                        return result;
                        
                        
        },
        R"pbdoc(
        Mie core shell simulation.
        
        This function simulates the radial profile of a spherical sample. The radial profile is computed at the given angles.
        
        :param radius: radius of the whole sample.
        :param delta: :math:`\delta` value of the sample
        :param beta: :math:`\beta` value of the sample           
        :param angle: maximum scattering angle
        :param mask: 2D square array. Set to 1 to get the scattering, 0 to skip. The output diffraction pattern will have the same size.
        :param center: 2-element array containing the x and y position, in pixels, relative to the center. If [0,0] is provided, the the center is assumed to be in the exact middle of the pattern.
        
        
        :returns: the scattering image, resulting from the simulation with the given parameters.
           )pbdoc", py::arg("radius"),  py::arg("delta"), py::arg("beta"), py::arg("angle"), py::arg("mask"), py::arg("center")=std::vector<double>({0,0}));
        
        
        
        mm.def("sphere_pattern_q", [](double radius, double delta_core, double beta_core, double maxangle, int size){
                        py::array_t<double> result = py::array_t<double>(size*size);
                        py::buffer_info buf = result.request();
                        double *ptr = (double *) buf.ptr;
                        
                        std::vector<double> angles(size*size);
//                         double dtheta=2.*maxangle/(double(size));
                        double K=1.;
                        double maxangle_rad=M_PI*maxangle/180.;
//                         double qmax = K*2.*std::sin(maxangle_rad/2.);
                        double qmax = K*std::sin(maxangle_rad); ///// IMPORTANT: WROOOOOONG!!!!

                        double dq= 2.*qmax/double(size);
                        
                        
                        for(int i=0; i<size*size;i++){
                            double x = double(i%size-size/2);
                            double y = double(i/size-size/2);
                            double q = std::sqrt(x*x+y*y)*dq;
                            angles[i]= (std::asin(q/K))/M_PI*180.;
                        }
                        
                        
                        auto intensities=mie::core_shell(
                                2.*M_PI*radius, 1-delta_core, -beta_core,
                                2.*M_PI*radius, 1-delta_core, -beta_core,
                                angles);
                        for(int i=0; i<size*size;i++) ptr[i]=intensities[i];
               
                        result.resize({size,size});
                        return result;
                        
                        
        },
        R"pbdoc(
        Mie core shell simulation.
        
        This function simulates the radial profile of a spherical sample. The radial profile is computed at the given angles.
        
        :param radius: radius of the whole sample.
        :param delta: :math:`\delta` value of the sample
        :param beta: :math:`\beta` value of the sample           
        :param angle: maximum scattering angle
        :param resolution: pattern size, in pixels
        
        
        :returns: the scattering image, resulting from the simulation with the given parameters. Coordinates are proportional to the q components on x and y.
           )pbdoc", py::arg("radius"),  py::arg("delta"), py::arg("beta"), py::arg("angle"), py::arg("resolution")=512);
        
        mm.def("core_shell_pattern", [](double radius, double thickness, double delta_core, double beta_core, double delta_shell, double beta_shell, double maxangle, int size){
                        py::array_t<double> result = py::array_t<double>(size*size);
                        py::buffer_info buf = result.request();
                        double *ptr = (double *) buf.ptr;
                        
                        std::vector<double> angles(size*size);
                        double dtheta=2.*maxangle/(double(size));
                        for(int i=0; i<size*size;i++){
                            double x = double(i%size-size/2);
                            double y = double(i/size-size/2);
                            angles[i]=std::sqrt(x*x+y*y)*dtheta;
                        }
                        
                        
                        auto intensities=mie::core_shell(
                                2.*M_PI*(radius-thickness), 1-delta_core, -beta_core,
                                2.*M_PI*radius, 1-delta_shell, -beta_shell,
                                angles);
                        for(int i=0; i<size*size;i++) ptr[i]=intensities[i];
               
                        result.resize({size,size});
                        return result;
                        
                        
        },
        R"pbdoc(
        Mie core shell simulation.
        
        This function simulates the radial profile of a spherical sample. The radial profile is computed at the given angles.
        
        :param radius: radius of the whole sample.
        :param thickness: thickness of the shell.
        :param delta_core: :math:`\delta` value of the core
        :param beta_core: :math:`\beta` value of the core        
        :param delta_shell: :math:`\delta` value of the shell
        :param beta_shell: :math:`\beta` value of the shell            
        :param angle: maximum scattering angle
        :param resolution: pattern size, in pixels
        
        
        :returns: the scattering image, resulting from the simulation with the given parameters.
           )pbdoc",  py::arg("radius"), py::arg("thickness"), py::arg("delta_core"), py::arg("beta_core"), py::arg("delta_shell"), py::arg("beta_shell"), py::arg("angle"), py::arg("resolution")=512);
        
        
        
}
