#!/usr/bin/env python3
import argparse
import os
import time
import sys
import pathlib
import shutil
import filecmp

done_ext = ".isdone"
copied_ext = ".iscopied"


def clean_files(basepath):
    basefolder = pathlib.Path(basepath)
    copiedfilefulllist = list(basefolder.rglob("*"+copied_ext))
        
    files = {}
    
    
    for copiedfilefull in copiedfilefulllist:

        abspath, copiedfilename = os.path.split(os.path.abspath(copiedfilefull))
        
        datafilename = copiedfilename[0:].replace(copied_ext, "")
        donefilename = copiedfilename.replace(copied_ext,done_ext)
        
        datafilefull = os.path.join(abspath, datafilename)
        donefilefull = os.path.join(abspath, donefilename)

        
        if os.path.isdir(datafilefull):  
            print("Removing directory ", datafilefull)
            shutil.rmtree(datafilefull)

        else:  
            print("Removing file ", datafilefull)
            os.remove(datafilefull)
        
        os.remove(donefilefull)
        os.remove(copiedfilefull)
        
        
    return files

def get_files(basepath):
    basefolder = pathlib.Path(basepath)
    donefilesfull = list(basefolder.rglob("*"+done_ext))
        
    files = {}
    
    
    for ff in donefilesfull:
        #print(os.path.split(os.path.abspath(ff)))
        abspath, donefilename = os.path.split(os.path.abspath(ff))
        relpath, donefilename = os.path.split(os.path.relpath(ff, start=basepath))
        
        datafilename = donefilename[0:].replace(done_ext, "")
        copiedfilename = donefilename.replace(done_ext, copied_ext)
        
        datafilefull = os.path.join(abspath, datafilename)
        
        if not os.path.exists(os.path.join(abspath, copiedfilename)):
            files[datafilefull]=[datafilename, relpath, abspath, donefilename, copiedfilename]
        
    return files




def listen(basepath, blacklist):
    first_loop=True
    first_wait=True
    timed_out=False
    t0=time.time()
    validsize=0
    while 1:

        files = get_files(basepath)

        #print(files)


        bl = []
        for name in files.keys():
            if name in blacklist:
                bl.append(name)
                
        for bb in bl:
            del files[bb]
                

        if len(files.keys())>0:
            return files
        
        
        if first_wait:
            print("Waiting for files in ", basepath)
            print("Press Crtl-C to exit.")
            first_wait=False                

        time.sleep(2.)


def touch(fname):
    open(fname, 'a').close()

    #if os.path.exists(fname):
        #os.utime(fname, None)
    #else:
        #open(fname, 'a').close()

def transfer(fileinfo, basepath, destbasepath):

    destdir = os.path.join(destbasepath, fileinfo[1])
    dest = os.path.join(destdir, fileinfo[0])
    source = os.path.join(fileinfo[2], fileinfo[0])

    if os.path.exists(dest):
        print("ERROR! File", source, " already exists as", dest,". File is blacklisted and won't be copied.")
        return 0
        


    try:

        pathlib.Path(destdir).mkdir(parents=True, exist_ok=True) 

        
        
        if os.path.isdir(source):  
            print("Moving directory ", source, "to", dest)
            shutil.copytree(source, dest)

        else:  
            print("Moving file ", source, "to", dest)
            shutil.copy2(source, dest)
            

        return 1
    
    except Exception as X:
        print(X, "raised while copying file", source, ". File is blacklisted and won't be copied.")
        return 0
    

def same_dirs(a, b):
    """Check that structure and files are the same for directories a and b

    Args:
        a (str): The path to the first directory
        b (str): The path to the second directory
    """
    comp = filecmp.dircmp(a, b)
    common = sorted(comp.common)
    left = sorted(comp.left_list)
    right = sorted(comp.right_list)
    if left != common or right != common:
        return False
    if len(comp.diff_files):
        return False
    for subdir in comp.common_dirs:
        left_subdir = os.path.join(a, subdir)
        right_subdir = os.path.join(b, subdir)
        return same_dirs(left_subdir, right_subdir)
    return True
    
def check(fileinfo, basepath, destbasepath):
    
    destdir = os.path.join(destbasepath, fileinfo[1])
    dest = os.path.join(destdir, fileinfo[0])
    source = os.path.join(fileinfo[2], fileinfo[0])

    status = False
    
    if os.path.isdir(source):  
        print("Checking directory ", source, "vs", dest)
        status = same_dirs(source, dest)


    else:  
        print("Checking file ", source, "vs", dest)
        status = filecmp.cmp(source, dest, shallow=False)

    return status


def run_daemon():
    parser = argparse.ArgumentParser()

    # parser.add_argument("folders", help="Configuration file (.yml)")
    #parser.add_argument('folders', help="Files or folders to analyze", nargs='+')
    #parser.add_argument('--timing', dest='print_timing', action='store_const',
                        #const=True, default=False,
                        #help='print timing information')
    #parser.add_argument('--check_pids', dest='check_pids', action='store_const',
                        #const=True, default=False,
                        #help='print pid mismatch information')
    #parser.add_argument('--verbose', dest='verbose', action='store_const',
                        #const=True, default=False,
                        #help='print internal libraries outputs')
    #parser.add_argument('--brightest', dest='save_brightest', action='store_const',
                        #const=True, default=False,
                        #help='save 16 brightest analysed images')
    #parser.add_argument("--conf", help="configuration file. Overrides the OLAF_CONF env variable")
    #parser.add_argument("--runs",
                        #help="Executes the analysis only on specific run numbers. "
                             #"Runs can be a single string, or comma separated strings, "
                             #"or integer ranges")
    #parser.add_argument("--img_thres", nargs="?", type=float, default=0.,
                        #help="Threshold to use for the images instead of the adaptive method")
    #parser.add_argument("--tof_thres", nargs="?", type=float, default=0.,
                        #help="Threshold to use for the TOF integrals instead of the adaptive method")
    ## parser.add_argument("--version", help="version of the analysis. This argument will replace the VERSION keyword in the configuration file")
    #parser.add_argument('--overwrite', dest='overwrite', action='store_const',
                        #const=True, default=False,
                        #help='silently overwrite existent analysis files')

    parser.add_argument("sourcebasepath", help="Source base path from which structure is replicated")
    parser.add_argument("destbasepath", help="Destination base path to which structure is replicated")

    #parser.add_argument("-s","--sourcebasepath", help="source base path from which structure is replicated")
    #parser.add_argument("-d","--destbasepath", help="destination base path from which structure is replicated")
    parser.add_argument('--clean', dest='clean', action='store_const',
                        const=True, default=False,
                        help='Clean up transferred files at the source after copy.')
    parser.add_argument('--check', dest='check', action='store_const',
                        const=True, default=False,
                        help='Check if files are the same after copy.')
    parser.add_argument('--purge', dest='purge', action='store_const',
                        const=True, default=False,
                        help='Remove all files marked as copied and returns.')
    args = parser.parse_args()

    sourcebasepath = args.sourcebasepath
    destbasepath = args.destbasepath

    
    blacklist = []
    
    if args.purge:
        print("Removing already transferred files...")
        clean_files(sourcebasepath)        
        return 0
    
    while 1:
        files = listen(sourcebasepath, blacklist)
    
        for f in files.keys():
            success = transfer(files[f], sourcebasepath, destbasepath)
            
            if not success:
                blacklist.append(f)
                
            else:
                if args.check:
                    success = check(files[f], sourcebasepath, destbasepath)
                    
                if success:
                    iscopiedname = os.path.join(files[f][2], files[f][4])
                    touch(iscopiedname)
                
                else:
                    print("WARNING: File", os.path.join(files[f][2], files[f][0]),"was not properly copied. File copy is rescheduled.")
                #print(f, files[f])
        
        if args.clean:
            clean_files(sourcebasepath) 
    

    
if __name__ == '__main__':
    run_daemon()
