from setuptools import setup
import sys
import setuptools

__version__ = '0.1'



setup(  name='nuxtransfer',
        version='0.1',
        description='Tool for moving the data from the DAQ to the nux-data storage',
        url='#',
        author='Alessandro Colombo',
        author_email='alcolombo@phys.ethz.ch',
        license='MIT',
        packages=['nuxtransfer'],
        entry_points = {
            'console_scripts': ['nuxtransfer=nuxtransfer.transfertool:run_daemon'],
        },
        zip_safe=False,
        install_requires=[
                        'argparse',
                        'pathlib'
                        ]

)
